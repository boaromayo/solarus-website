'use strict';

const DEFAULT_LANGUAGE = '{{ .Site.Params.defaultLanguageCode }}';
const CONTACT_EMAIL = '{{ .Site.Params.contactEmail }}';

// Dirty hack to detect if we are running on GitLab Pages.
const IS_GITLAB_PAGES = window.location.origin.includes('gitlab.io');
