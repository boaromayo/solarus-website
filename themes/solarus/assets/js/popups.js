'use strict';

document.addEventListener('DOMContentLoaded', function () {
  const id = 'language-dropdown';
  const dropdown = document.getElementById(id);
  const popup = document.querySelector(`[data-dropdown=${id}]`);
  if (!dropdown || !popup) return;

  let hasEnteredPopup = false;

  const setPopupVisible = (visible) => {
    if (visible) {
      const rect = dropdown.getBoundingClientRect();
      popup.style.left = `${rect.left}px`;
      popup.style.top = `${rect.bottom}px`;
      popup.style.display = 'block';
    } else {
      hasEnteredPopup = false;
      popup.style.display = 'none';
    }
  };

  const isInsideNode = (node, x, y) => {
    const rect = node.getBoundingClientRect();
    return x >= rect.left && x <= rect.right && y >= rect.top && y <= rect.bottom;
  };

  dropdown.addEventListener('mouseenter', () => {
    setPopupVisible(true);
  });
  dropdown.addEventListener('mouseleave', () => {
    if (!hasEnteredPopup) {
      setPopupVisible(false);
    }
  });
  dropdown.addEventListener('click', () => {
    hasEnteredPopup = true;
    setPopupVisible(true);
  });

  popup.addEventListener('mouseenter', () => {
    hasEnteredPopup = true;
    setPopupVisible(true);
  });
  popup.addEventListener('mouseleave', () => {
    setPopupVisible(false);
  });

  document.addEventListener('click', (evt) => {
    const x = evt.x;
    const y = evt.y;
    const outsidePopup = !isInsideNode(popup, x, y) && !isInsideNode(dropdown, x, y);
    if (outsidePopup) {
      setPopupVisible(false);
    }
  });
});
