---
title: Press Kit
excerpt: Press kit to facilitate communication about Solarus.
tags: [press, kit, presskit, toolkit, branding, communication, journalist, reporter]
aliases:
  - /presskit
  - /press
  - /about/press
  - /about/presskit
  - /en/about/press
  - /en/about/presskit
---

## Wording

When you present Solarus, you may use the following wording:

> Solarus is a multiplatform, free and open-source 2D game engine, written in C++. It is primarly dedicated to Action-RPGs, also called _Zelda-likes_, but can actually do much more.

Notes:

- Please avoid presenting Solarus as a Zelda engine. It has built-in features for Zelda-like games for historical reasons, but you can do much more!
- Please avoid presenting Solarus as Super Nintendo™ emulator or ROM hack. It is an engine buit from scratch.

## Branding

Always prefer the frame versions over the frameless ones.

### Logotype

{{< branding-logo background="dark" file="solarus_new_logo_dark" caption="For dark backgrounds" >}}

{{< branding-logo background="light" file="solarus_new_logo_white" caption="For light backgrounds" >}}

{{< branding-logo background="dark" file="solarus_new_logo_white_noframe" caption="For dark backgrounds" >}}

{{< branding-logo background="light" file="solarus_new_logo_dark_noframe" caption="For white backgrounds" >}}

### Mark

{{< branding-logo background="light" file="solarus_new_logo_mark_frame" caption="For white backgrounds" class="small" >}}

{{< branding-logo background="dark" file="solarus_new_logo_mark_white_frame" caption="For dark backgrounds" class="small" >}}

{{< branding-logo background="dark" file="solarus_new_logo_mark_noframe" caption="For all backgrounds" class="small" >}}

### Colors

{{< branding-colors >}}

### Fonts

{{< branding-fonts >}}

## Product screenshots

### Engine

![Children of Solarus screenshot](engine/children-of-solarus-1.png)
![Children of Solarus screenshot](engine/children-of-solarus-2.png)
![Children of Solarus screenshot](engine/children-of-solarus-3.png)

### Editor

![Solarus Quest Editor - Map Editor](editor/editor-screenshot-map.png)
![Solarus Quest Editor - Entity Editor](editor/editor-screenshot-entity.png)
![Solarus Quest Editor - Dialogs Editor](editor/editor-screenshot-dialogs.png)
![Solarus Quest Editor - Sprite Editor](editor/editor-screenshot-sprite.png)

### Launcher

Coming soon.
