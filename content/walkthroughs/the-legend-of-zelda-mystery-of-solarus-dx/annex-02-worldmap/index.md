---
excerpt: 'Global view of the outside world. You will find here the list of enemies in each zones and the availables locations.'
tags: []
title: 'Annex: World Map'
---

Here is a generic view of the outside world of the game. You will find here the list of enemies in each zones and the availables locations.

## West Mount Terror (A1)

![Zone map](a1-west-mount-terror.png)

### Enemies

- Blue Dogman x3
- Red Dogman x2

### Places

1. West Cavern Entrance - 1F
1. West Cavern Exit - 2F (West exit)
1. West Cavern Exit - 1F (East exit)
1. Crystal Temple (Level 7)
1. West Cavern Exit - 2F (East exit)
1. Lasers Cave
1. West Cavern Entrance - 3F
1. West Cavern Exit - 3F

## East Mount Terror (B1)

![Zone map](b1-east-mount-terror.png)

### Enemies

- Blue Dogman x2
- Red Dogman x2

### Places

1. North Fairy Fountain
1. East Cavern Exit - 4F
1. East Cavern Exit - 5F
1. Optical Illusion Cavern
1. Optical Illusion Cavern Exit
1. Rock Peaks Dungeon (Level 8)

## Ancient Castle (A2)

![Zone map](a2-ancient-castle.png)

### Enemies

- Blue Knight x2
- Red Knight x1

### Places

1. Ancient Castle Main Entrance (Level 5)
1. Jail Entrance
1. Waterfall Hideout Entrance
1. Ancient Castle Roof Exit
1. Ancient Castle West Wing Exit
1. Ancient Castle East Wing Entrance
1. True Hero Labyrinth Entrance
1. True Hero Labyrinth Exit

## Witch's Hut (B2)

![Zone map](b2-witch-hut.png)

### Enemies

- Blue Knight x2
- Red Knight x1

### Places

1. River Labyrinth Entrance
1. West Channel Entrance Entrée Ouest du Canal
1. East Channel Entrance Entrée Est du Canal
1. Witch's Hut
1. Inferno's Daedalus (Level 6)
1. Bazaar
1. Village's Fairy Fountain Exit
1. Village's Fairy Fountain Entrance - 2F
1. Waterfall Tunnel
1. East Cavern Entrance - 1F
1. East Cavern Exit - 2F
1. East Cavern Exit - 3F
1. East Cavern Entrance - 2F

## West Lyriann (A3)

![Zone map](a3-west-lyriann.png)

### Enemies

- Soldier x3
- Green Knight x2
- Blue Knight x1
- Red Knight x1

### Places

1. Link's House
1. Store / Armory
1. Sahasrahla's House
1. Bakery
1. Blacksmith's Cave
1. Hole and Flower Cave
1. Telepatic Closet
1. Roc's Cavern (Level 2)
1. Bomb Cave
1. Master Arbror's Den (Level 3)
1. Master Arbror's Den Secret Entrance

## East Lyriann (B3)

![Zone map](b3-east-lyriann.png)

### Enemies

- Soldier x2
- Green Knight x2
- Blue Knight x1
- Red Knight x1

### Places

1. Rupees House
1. Granny Lyly's House
1. Lyriann Cave
1. Fairy Foutain of Lyriann
1. Bone Key Cave
1. Lost Woods Cave
1. Chests Cave
1. Waterfall Tunnel

## Skyward Tower (C3)

![Zone map](c3-skyward-tower.png)

### Enemies

- Ropa x2
- Snapdragon x3

### Places

1. Skyward Tower (Secret Level)

## South Forest (A4)

![Zone map](a4-south-forest.png)

### Enemies

- Soldier x4
- Green Knight x1
- Blue Knight x1

### Places

1. Forest Dungeon (Level 1)
1. Secret exit of Forest Dungeon
1. Twin Caves
1. Twin Caves
1. To Beaumont's Palace (Level 4)
1. Lake West Cave Entrance
1. Lake West Cave Exit
1. Teleport to the Shrine of Memories (Level 9)
1. West Exit Shrine of Memories - 2F
1. East Exit Shrine of Memories - 2F

## Lake (B4)

![Zone map](b4-lake.png)

### Enemies

- Green Knight x1
- Blue Knight x1
- Red Knight x1

### Places

1. Lake's Shop
1. Lake's Shop Secret Exit
1. Surprise Wall
1. Billy the Reckless's Den
1. Cave under the Waterfall Hidden Exit
1. Cave under the Waterfall Exit
1. Lake East Cave
