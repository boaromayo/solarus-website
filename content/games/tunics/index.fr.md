---
excerpt: Un Rogue-like généré aléatoirement dans l'univers de Zelda.
---

### Présentation

_Tunics!_ est un Rogue-like dans l'univers de _The Legend of Zelda_. Cela implique donc qu'il n'y a que des donjons, et que ceux-ci sont générés aléatoirement. De surcroît, lorsque vous mourrez, vous devez redémarrer depuis le début, et vous perdez toutes les améliorations acquises (armes, vies, etc). Ce jeu doit être considéré comme une preuve de concept très aboutie.
