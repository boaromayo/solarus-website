---
age: 'all'
controls: ['keyboard', 'gamepad']
developer: Legofarmen
download: 'https://gitlab.com/syllan-games/patched-tunics/-/jobs/artifacts/0.3+ci/raw/tunics-0.3+ci.solarus?job=quest-package'
excerpt: A randomly generated Rogue-like in the Zelda universe.
genre: ['Rogue-like']
id: tunics
languages: ['en']
license: ['GPL v3', 'CC-BY-SA 4.0', 'Proprietary (Fair use)']
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2015-09-09
latestUpdateDate: 2021-09-28
screenshots: ['screen1.png', 'screen2.png', 'screen3.png', 'screen4.png']
solarusVersion: 1.5.3
sourceCode: 'https://gitlab.com/syllan-games/patched-tunics'
thumbnail: 'thumbnail.png'
title: Tunics!
version: 0.3+ci
---

### Presentation

_Tunics!_ is a Rogue-like _Legend of Zelda_ quest. It means it's only dungeons, and they are randomly generated. Moreover, once you're dead, you'll have to restart from the beginning, losing all your improvements (weapons, health, etc). It should be considered a well-polished proof-of-concept.
