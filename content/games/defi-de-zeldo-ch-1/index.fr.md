---
excerpt: Zeldo, un créateur, défie Link dans un donjon qu'il a créé. Link arrivera t-il à s'en sortir ?
---

ZeldoRetro a créé en 2017 un court jeu _The Legend of Zelda_ pour défier son ami Adenothe. Ce n'était pas destiné à être publié publiquement. Cependant, la sortie du second chapitre l'a fait reconsidérer cette décision, car tout le monde lui disait "Attendez ? Si ceci est le chapitre 2, où est le chapitre 1 ?". Le voici !

Ce jeu est le premier épisode dans la série de ZeldoRetro _Le Défi de Zeldo_. Ce premier chapitre est intitulé _La Revanche du Bingo_, est est bien moins ambitieux que sa suite. Il ne contient qu'un seul donjon, et pas de monde à explorer, ce qui le rend plutôt court. Il se finit en moins d'une heure.
