---
date: '2001-08-20'
excerpt: Salut à tous ! Après 10 jours de vacances bien méritées, je suis de retour ! Fin des vacances = retour des mises à jour... Vous allez vite le...
tags:
- solarus
title: Fin des vacances !
---

Salut à tous !

Après 10 jours de vacances bien méritées, je suis de retour ! Fin des vacances = retour des mises à jour... Vous allez vite le constater.

Autre bonne nouvelle : Thomas va rentrer le 26 août. Donc les nouveaux chipsets arriveront peu de temps après, ce qui promet de nouveaux screenshots inédits !

Enfin, je tiens à vous remercier tous de votre intérêt pour le jeu. Depuis l'ouverture du site, vous avez téléchargé la démo... 64 fois ! C'est grâce à vous que je trouve l'énergie de continuer (je ne sais pas d'où je sors ça) !
