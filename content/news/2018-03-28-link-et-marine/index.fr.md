---
date: '2018-03-28'
excerpt: Cette semaine, nous avons beaucoup travaillé sur la suite de la quête principale. Cela avance maintenant vraiment très vite ! Nous avons du...
tags:
- solarus
title: Link et Marine
---

Cette semaine, nous avons beaucoup travaillé sur la suite de la quête principale. Cela avance maintenant vraiment très vite !

Nous avons du coup pas mal bossé sur certaines cinématiques du jeu dont celle de Marine et Link qui jouent de la musique ensemble.

![](1-2-300x240.png)

De plus, ce soir, un live Making a lieu à 21h00 afin de vous montrer le travail réalisé. Cela se passe à cette adresse : <https://www.youtube.com/watch?v=1wML92aOK1s>
Si cela vous intéresse, n'hésitez pas à venir. L'émotion et la bonne humeur seront bien entendu de la partie !
