---
date: '2013-04-23'
excerpt: I have just reorganized the git repository of Solarus. Our quests are now in separated git repositories. The main repository...
tags:
  - solarus
title: Separating projects
---

I have just reorganized the git repository of Solarus. Our quests are now in separated git repositories. The main repository (<https://github.com/christopho/solarus>) no longer contains quests. It contains the engine, the map editor and some other tools.

Here are the new repositories for our quests:

- Zelda Mystery of Solarus DX: <https://github.com/christopho/zsdx>
- Zelda Mystery of Solarus XD: <https://github.com/christopho/zsxd>
- Zelda Mercuris' Chest: <https://github.com/christopho/zelda_mercuris_chest>

What? Zelda Mercuris' Chest? What is that?

It is our new project in development! More details will come soon. But here is a first screenshot:

![](zmc_mine_2.png "Mercuris' Chest - Rail Temple")
