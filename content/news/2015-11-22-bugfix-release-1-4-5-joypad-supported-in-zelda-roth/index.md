---
date: '2015-11-22'
excerpt: A new bugfix release of Solarus, Solarus Quest Editor and our games was just published! A bunch of bugs were fixed. Zelda Return of the Hylian can...
tags:
- solarus
title: Bugfix release 1.4.5, joypad supported in Zelda ROTH
---

A new bugfix release of Solarus, Solarus Quest Editor and our games was just published!

A bunch of bugs were fixed. [Zelda Return of the Hylian](http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/) can now be played with a joypad, and commands can be customized!

![](large)

Also, [Zelda Mystery of Solarus DX](http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/) is now available in Italian (beta) thanks to Marco!

- [Download Solarus 1.4](http://www.solarus-games.org/engine/download/ "Download Solarus")
- [Solarus Quest Editor](http://www.solarus-games.org/engine/solarus-quest-editor/)
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html "LUA API documentation")

## Changes in Solarus 1.4.5

- Fix file name not shown when there is an error in dialogs file (#718).
- Fix saving special characters in data files (#719).
- Fix sol.main.load_file() returning a string instead of nil on error (#730).
- Fix performance issue when sprites have huge frame delays (#723).
- Fix collisions triggered for removed entities (#710).
- Fix hero disappearing if lifting animation has less than 5 frames (#682).
- Fix collisions with diagonal dynamic tiles larger than 8x8 (#486).
- Fix path finding movement not working with NPCs (#708).
- Fix stuck on non-traversable dynamic tiles covered by traversables (#769).
- Fix collision detection of custom entities that do not move.
- Fix pickables with special movement falling in holes too early.
- Fix blocking streams not working when the hero's speed is greater (#488).

## Changes in Solarus Quest Editor 1.4.5

- Add keyboard shortcut (F4) to switch between map view and map script (#75).
- Map editor: fix entity being moved after closing its dialog (#76).
- Map editor: start selection when clicking a tile with control/shift (#47).
- Map editor: synchronize tile patterns selection from map selection (#35).
- Sprite editor: improve auto-selection after removing a direction (#70).
- Dialogs/strings editor: add a duplicate button (#72).
- Dialogs/strings editor: Shows also missing marks on parent nodes (#68).
- Fix crash on FreeBSD when running the quest (#112).
- Fix crash on Mac OS X 64 bit.

## Changes in Zelda ROTH SE 1.0.7

- Allow to customize keyboard and joypad controls (#2).
- Add title screen music (#74).
- Add left/right arrows in pause menus to indicate possible actions (#60).
- Show the current floor when it changes.
- Fix typo in French dialogs in the intro.
- Fix the blue potion dialog in English (#78).
- Fix entities falling in holes even when hooked to the hookshot.
- Fix caps lock state ignored after restarting the game (#20).
- Fix hero stuck in a cliff near shadow village.
- Fix hammer stakes pushed too early (#65).
- Fix enemies active in other regions before taking the first separator (#72).
- Fix enemy projectiles not removed when taking a separator (#71).
- Fix saving with life 0 during the game-over sequence (#82).
- Fix collisions of fire rod and ice rod (#55).
- Fix torches staying on after game over.
- Fix weak cavern wall already open in south-east.
- Make the hammer activate crystals (#75).
- Improve moving the camera with Ctrl+direction (#34).
- Remember the last savegame played.
- Dungeon 5: fix falling hands not properly removed.
- Dungeon 7: fix a door not correctly aligned.
- Dungeon 9: fix door closed when leaving without Zelda after boss key (#79).
- Dungeon 9: fix boss door reopening after killing him (#81).
- Dungeon 9: update soldier sprites to the original ones.

## Changes in ZSDX 1.10.2

- Add Italian translation (thanks Marco).
- Make fairies play a sound when restoring life even when already full (#101).
- Fix dialog cursor drawn at wrong position after successive questions (#105).
- Dungeon 1: fix unintentional extra difficulty with block alignment (#102).
- Dungeon 3: fix enemy entering mini-boss room (#107).
- Dungeon 9: fix spikes sending back to wrong room (#108).
- Dungeon 10: fix evil tiles door not opening sometimes (#94).
- End screen: fix freezing the hero (#109).

## Changes in ZSXD 1.10.2

- Make fairies play a sound when restoring life even when already full (#101).

Enjoy!
