---
date: '2018-11-03'
excerpt: Vous êtes présents sur Discord ? Si oui, vous serez ravis d'apprendre que Solarus a désormais son salon de discussion officiel sur Discord, le...
tags:
- solarus
title: Rejoignez-nous sur le chat Discord Solarus !
---

Vous êtes présents sur Discord ? Si oui, vous serez ravis d'apprendre que Solarus a désormais son salon de discussion officiel sur Discord, le chat des gamers.

Rejoignez-nous pour parler de Zelda, de la création de jeux avec Solarus, de vos projets ou des nôtres, de Minecraft, des IRLs ou de ce que vous voulez qui n'a rien à voir !

- [Salon de discussion Solarus sur Discord](https://discord.gg/yYHjJHt)

Venez nombreux. On vous attend !
