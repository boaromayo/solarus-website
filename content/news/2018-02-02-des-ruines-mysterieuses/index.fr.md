---
date: '2018-02-02'
excerpt: Aujourd'hui, nous prenons la direction des terres à l'est du monde de Mercuris' Chest avec cette image d'intrigantes ruines et de ce...
tags:
  - solarus
title: Des ruines mystérieuses
---

Aujourd'hui, nous prenons la direction des terres à l'est du monde de Mercuris' Chest avec cette image dintrigantes ruines et de ce temple.

Pour l'instant, le projet est un peu en pause afin d'aider Zeldaforce dans sa quête des huit instruments à travers A Link to the Dream mais il repartira de plus belle sous peu ;)

À bientôt pour de prochaines news.

![](MC-ruinesest-300x227.png)
