---
date: '2005-02-22'
excerpt: Nous vous le promettions depuis longtemps, et il est enfin là ! Admirez le premier artwork de Link pour Zelda M'C, dessiné par Neovyze...
tags:
- solarus
title: 'Zelda M''C : un artwork de Link !'
---

Nous vous le promettions depuis longtemps, et il est enfin là ! Admirez le premier artwork de Link pour Zelda M'C, dessiné par Neovyze :

![](link1_mini.jpg)

Nous souhaitons connaître votre avis sur le style de Link ! Alors n'hésitez pas à réagir en postant vos commentaires et en répondant au sondage sur la droite de la page :)
