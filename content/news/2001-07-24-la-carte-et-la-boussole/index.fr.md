---
date: '2001-07-24'
excerpt: Salut à tous ! Une mise à jour pour vous présenter le nouveau système de Carte / Boussole. Dans la démo, il faut bien reconnaùtre que la Carte...
tags:
  - solarus
title: La Carte et la Boussole
---

Salut à tous !

Une mise à jour pour vous présenter le nouveau système de Carte / Boussole. Dans la [démo](http://www.zelda-solarus.com/demo.php3), il faut bien reconnaître que la Carte et la Boussole des donjons sont loin d'être géniales. Et c'est pour cette raison que Thomas a redessiné les Cartes et les Boussoles du jeu en s'inspirant de Zelda 3. Voici les screenshots :

[](solarus-ecran33.png)

[](solarus-ecran34.png)

[](solarus-ecran35.png)

C'est mieux non ? Sur deux de ces photos vous pouvez reconnaître le RC et le 1er étage du Donjon de la Forêt.
