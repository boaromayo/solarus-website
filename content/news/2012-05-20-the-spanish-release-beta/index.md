---
date: '2012-05-20'
excerpt: A new version of Zelda Mystery of Solarus DX(version 1.5.1) is now available. It includes a beta version of the Spanish...
tags:
  - solarus
title: The Spanish release (beta)!
---

A new version of [Zelda Mystery of Solarus DX](http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/ 'Zelda Mystery of Solarus DX')(version 1.5.1) is now available. It includes a beta version of the Spanish translation!

Changes:

- Spanish translation (beta)
- Fix some issues in the English translation
- Skyward Tower: the hero could get stuck in a wall
- Skyward Tower: other minor improvements
- Fix blue flames shot by some bosses and that were stuck sometimes
- Ancient Castle: minor improvements
- Inferno Maze: a puzzle could be skipped

[Download now](http://www.zelda-solarus.com/jeu-zsdx-download&lang=en) on Zelda Solarus

I would like to thank Clow_eriol, Emujioda, LuisCa, Musty, Xadou, Guopich and falvarez for their contribution to the Spanish translation. It still needs more testing and verifications. Please [contact me](http://www.solarus-games.org/contact/ 'Contact') if you want to help!
