---
date: '2013-04-07'
excerpt: 'Une nouvelle rubrique vient de faire son apparition : les musiques originales (OST) de Zelda Mystery of Solarus DX ! Cinq compositions inédites...'
tags:
- solarus
title: Musiques originales de Zelda Mystery of Solarus DX
---

Une nouvelle rubrique vient de faire son apparition : les musiques originales (OST) de Zelda Mystery of Solarus DX !

Cinq compositions inédites ont été créées spécialement pour le jeu par Marine et Metallizer. De la Montagne des Terreurs à la Tour des Cieux en passant par l'inoubliable Temple des Souvenirs, vous pouvez les écouter et les les réécouter à volonté !

- [Musiques originales](http://www.zelda-solarus.com/zs/article/zmosdx-musiques/)
