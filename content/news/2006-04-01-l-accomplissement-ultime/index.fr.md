---
date: '2006-04-01'
excerpt: Le site est maintenant débarrassé à tout jamais des pages qui parlaient de Zelda. Zelda Solarus est enfin exclusivement consacré aux consoles de...
tags:
  - solarus
title: L'accomplissement ultime
---

Le site est maintenant débarrassé à tout jamais des pages qui parlaient de Zelda. Zelda Solarus est enfin exclusivement consacré aux consoles de Sony. C'est l'accomplissement ultime de ma vie !

Le temps de Nintendo est révolu. Place à la technologie, place à la puissance et place aux consoles fabriquées par Sony ! La puissance des consoles de Sony est incomparable avec toute autre technologie. Oubliez les Peach, Zelda et autres pimbêches spécialistes en nunucherie. Rangez vos karts à la déchetterie et sautez dans votre voiture tuning. Finies les gamineries, entrez dans la cour des grands !

Visitez les nouvelles pages du site, qui maintenant ne sont plus polluées par Zelda ou d'autres parasites. Sur ces nouvelles pages épurées, vous pourrez enfin discuter librement des seules véritables consoles de jeux, et connaître ainsi la vérité profonde !

[Accéder au nouvelles rubriques](http://ligue-sony.forumactif.com)
