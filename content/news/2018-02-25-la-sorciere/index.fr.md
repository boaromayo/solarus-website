---
date: '2018-02-25'
excerpt: Cette semaine, pour le projet de remake de Link's Awakening, on vous présente l'unique sorcière de Cocolint. Pour ceux qui ne connaissent pas, il...
tags:
- solarus
title: La sorcière
---

Cette semaine, pour le projet de remake de Link's Awakening, on vous présente l'unique sorcière de Cocolint. Pour ceux qui ne connaissent pas, il s'agit d'un personnage important du jeu. :)

![](2-3-300x240.png)
![](1-3-300x240.png)

Comme d'habitude, n'hésitez pas à nous laisser un petit commentaire.
