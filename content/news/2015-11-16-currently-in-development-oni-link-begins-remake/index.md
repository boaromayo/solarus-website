---
date: '2015-11-16'
excerpt: ' Christopho and Mymy have begun to work on Oni Link Begins, the sequel to Return of the Hylian. This is also an original game by Vincent Jouillat,...'
tags:
- solarus
title: 'Currently in development : Oni Link Begins remake'
---

![olb_se_logo](olb_se_logo-1024x700.png)

Christopho and Mymy have begun to work on *Oni Link Begins*, the sequel to *Return of the Hylian*. This is also an original game by Vincent Jouillat, and the second part of his trilogy. There is no release date announced, but you can watch the progression of the remake.

- Github: <https://github.com/christopho/zelda_olb_se>
- Mapping videos: [part 1](https://www.youtube.com/watch?v=d9_E6c7lYOY), [part 2](https://www.youtube.com/watch?v=SvwLzam1jVA)
