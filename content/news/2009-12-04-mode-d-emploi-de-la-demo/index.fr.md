---
date: '2009-12-04'
excerpt: Bonsoir à tous, A deux semaines de la sortie de la démo de Zelda Mystery of Solarus DX, pour vous faire patienter, nous vous proposons dès...
tags:
  - solarus
title: Mode d'emploi de la démo
---

Bonsoir à tous,

A deux semaines de la sortie de la démo de Zelda Mystery of Solarus DX, pour vous faire patienter, nous vous proposons dès maintenant de lire... le mode d'emploi !

Il a été réalisé graphiquement par Neovyse, et en ce qui me concerne, je trouve que c'est du travail de pro :P.

- [Télécharger le mode d'emploi](http://www.zelda-solarus.com/zsdx/mode_emploi_demo.pdf)
