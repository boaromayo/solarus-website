---
date: '2012-04-01'
excerpt: Notre jeu parodique Zelda Mystery of Solarus XD est disponible y a un an exactement. Il a été téléchargé un peu plus de 14000 fois. Pour...
tags:
- solarus
title: Zelda Mystery of Solarus XD fête ses 1 an !
---

Notre jeu parodique [Zelda Mystery of Solarus XD](http://www.zelda-solarus.com/jeu-zsxd-download) est disponible y a un an exactement. Il a été téléchargé un peu plus de 14000 fois.

![](boite_400.jpg)

Pour fêter ça, Binbin et moi avons décidé d'y rejouer et de nous enregister en vidéo ;)

Nous avons donc refait le jeu complètement, il y en a pour 2h16 de vidéo au total ! :D

Si vous n'y avez encore jamais joué, par exemple parce que vous avez téléchargé Zelda Solarus DX sans connaître Zelda Solarus XD, nous ne pouvons que vous conseiller de le [télécharger](http://www.zelda-solarus.com/jeu-zsxd-download) ^\_^. Autrement, ces vidéos vont vous rappeler des souvenirs.

À vos sabres laser !
