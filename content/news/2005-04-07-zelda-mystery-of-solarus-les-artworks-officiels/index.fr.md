---
date: '2005-04-07'
excerpt: ' Voici enfin les artworks officiels de Zelda Solarus (3 ans après tout de même !). Ils illustrent le mode d''emploi du jeu. Vu qu?ils ne...'
tags:
- solarus
title: 'Zelda : Mystery of Solarus : les artworks officiels !'
---

![](link_mort.jpg)

Voici enfin les artworks officiels de Zelda Solarus (3 ans après tout de même !).

Ils illustrent le [mode d'emploi](http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=notice) du jeu.

Vu qu?ils ne sont pas réalisés par Neovyze, « l?artworkeur » officiel de Mercuris? Chest, mais par moi, Gobbi, ils sont dans un style tout à fait différent, qui je l?espère vivement, vous plaira.

[Mode d'emploi illustré de Zelda : Mystery of Solarus](http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=notice)
