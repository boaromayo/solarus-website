---
date: '2001-10-14'
excerpt: 'Zelda : Mystery of Solarus commence à se faire connaùtre sur le web. J''ai inscrit le jeu au Alex d''Or, sorte de cérémonie des Césars réservée...'
tags:
- solarus
title: Zelda Solarus sur le web
---

Zelda : Mystery of Solarus commence à se faire connaître sur le web. J'ai inscrit le jeu au Alex d'Or, sorte de cérémonie des Césars réservée aux jeux amateurs. Bien sûr je n'ai pas vraiment d'ambition de remporter le prix d'une quelconque catégorie, au vu de la concurrence : plus de 40 participants à l'heure actuelle.

J'ai eu la bonne surprise de voir plusieurs sites de RPG parler de Zelda Solarus en proposant un lien vers la démo... Il y a par exemple le site de [King Edgar](http://www.kingedgar.fr.st) qui a fait un dossier sur les Alex d'or, avec un petit texte pour de nombreux jeux dont notamment Zelda Solarus. Visitez également [Zhell RPG](http://www.zhellrpg.fr.st), site contenant quelques tests RPG Maker dont celui de ZS. Il y a aussi [www.lalegendecontinue.fr.st](http://www.lalegendecontinue.fr.st), un site encore très vide de contenu mais qui a une page réservée à ZS.

Et il y a bien sûr le site de notre cher Netgamer : [Oxyd-Online](http://www.oxyd-online.fr.st), qui propose un large dossier sur ZS (avec des exclus et des screenshots que vous ne trouverez même pas ici !)

Enfin, si vous êtes un visiteur du [Forum](http://www.zelda-solarus.com/forum.php3) vous savez déjà que [Games Creators](http://www.games-creators.com) est en train de préparer un dossier complet sur les Zelda créés avec RPG Maker, dont bien sûr ZS.

Voilà... Si vous êtes vous-même webmaster d'un site et que vous voulez parler de Zelda : Mystery of Solarus, alors prévenez-moi sur le forum pour que je sois quand même au courant :-)
