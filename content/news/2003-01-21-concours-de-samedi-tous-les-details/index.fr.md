---
date: '2003-01-21'
excerpt: Salut à tous ! Comme vous le savez, le samedi 25 janvier à 21 h (15 h heure québécoise) est organisé un quizz sur Zelda Solarus sur le...
tags:
- solarus
title: 'Concours de samedi : tous les détails'
---

Salut à tous !

Comme vous le savez, le **samedi 25 janvier à 21 h (15 h heure québécoise)** est organisé un quizz sur Zelda Solarus sur le chat-IRC du site. Dans cette news interminable je vais vous parler en détail du déroulement et des règles de ce concours.

Le quizz commencera samedi à 21 h précises (15 h pour les québécois). Pour vous connecter au chat, il y a deux possibilités : soit vous y allez en cliquant sur [ce lien](http://connexion.asterochat.com/?id=23973) ou sur le lien "chat" dans le menu du site, et vous accédez tout de suite au chat si tout se passe bien, soit vous utilisez le logiciel mIRC pour venir. Les paramètres de mIRC sont les suivants : serveur=irc.epiknet.org, port=6667, puis vous tapez /join #zeldasolarus pour rejoindre le chat. mIRC est téléchargeable sur www.telecharger.com. En cas de problèmes de connexion envoyez-nous un [mail](http://www.zelda-solarus.com/mailto:christopho@zelda-solarus.net).

117 questions très exactement seront posées. Elles portent toutes sur Zelda : Mystery of Solarus. Elles sont très diverses et tout le monde à ses chances ! Certaines questions sont des questions normales, d'autres sont des mots à reconstituer (lettres mélangées). Tout se fait automatiquement, il ne peut donc pas y avoir d'erreurs ni de contestations. Pour chaque question, le premier qui donne la bonne réponse a un point. Il faut donner exactement la bonne réponse : si la réponse attendue est "grappin", la réponse "grapin" sera considérée comme fausse. Attention donc aux fautes d'orthographe ! Cependant les majuscules n'ont pas d'importance donc "Grappin" fonctionne. Et les déterminants (le, la, les, un...) peuvent être mis ou non, ça marche aussi. La réponse "le grappin" est donc elle aussi correcte.

Quoi qu'il en soit, si quelqu'un donne une réponse que l'on peut considérer comme bonne alors que le quizz ne l'a pas acceptée, il est toujours possible pour moi de la valider manuellement. Tout devrait donc se passer sans injustices :-)

Par ailleurs, le quizz commence à 21 h et se termine dès que toutes les questions ont été posées, mais vous pouvez arriver et partir à n'importe quelle heure, votre score sera quand même pris en compte et mémorisé. Le seul problème c'est que si vous n'êtes pas là du début à la fin, vous raterez des questions et vous aurez donc moins de chance d'être sur le podium.

En cas de flood intensif, de mauvais comportement ou de triche sous quelque forme que ce soit, il est bien évident que nous nous réservons le droit de kicker ou de bannir des personnes si elles nous y obligent. Tous les membres de l'équipe de ZS qui seront là ne participeront évidemment pas au concours mais ils seront modérateurs et seront là pour veiller au bon déroulement du quizz.

Pour finir parlons des lots. Les abonnés à la [newsletter](http://www.zelda-solarus.com/newsletter.php) le savent déjà : le grand gagnant recevra en avant-première (par la Poste ou en téléchargement) la démo de notre prochaine création, Zelda : Advanced Project ! Le deuxième et le troisième auront droit quant à eux à un CD des musiques de Zelda Solarus.

Voilà, je crois que tout est dit. Il ne me reste plus qu'à vous souhaiter bonne chance à tous ! Si vous avez des questions, n'hésitez pas à poster des commentaires.
