---
date: '2002-11-03'
excerpt: Salut à tous ! Comme vous le savez, Zelda Solarus 2 (titre provisoire) sera développé avec The Games Factory. Zelda Solarus 1 était assez...
tags:
- solarus
title: Comparatif entre TGF et la SNES
---

Salut à tous !

Comme vous le savez, Zelda Solarus 2 (titre provisoire) sera développé avec The Games Factory. Zelda Solarus 1 était assez limité techniquement, RPG Maker oblige. Netgamer vous a préparé un petit comparatif entre les performances graphiques et sonores de TGF et de la Super Nintendo. Si vous voulez en savoir plus, c'est [par ici](http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=power) !
