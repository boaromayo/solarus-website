---
date: '2013-04-06'
excerpt: 'Hello everyone, It is now easier to download the latest releases of our games. So far, there was no real download page here: you had to visit our...'
tags:
  - solarus
title: Download pages updated
---

Hello everyone,

It is now easier to download the latest releases of our games. So far, there was no real download page here: you had to visit our French Zelda website, even if its download pages were available in English. Now, there have proper download pages here on solarus-games.org. I have just added a new "Downloads" menu above, with new download pages for our games [Zelda Mystery of Solarus DX](http://www.solarus-games.org/downloads/download-zelda-mystery-of-solarus-dx/ 'Download ZSDX') and [Zelda Mystery of Solarus XD](http://www.solarus-games.org/downloads/download-zelda-mystery-of-solarus-xd/ 'Download ZSXD'). All systems are listed with the necessary details.
