---
date: '2001-10-15'
excerpt: Le cinquième donjon a beaucoup avancé ce week-end et hier. Pour tout vous dire je suis en train de m'occuper du mini-boss, et après cela il ne me...
tags:
- solarus
title: Quoi de neuf ?
---

Le cinquième donjon a beaucoup avancé ce week-end et hier. Pour tout vous dire je suis en train de m'occuper du mini-boss, et après cela il ne me restera plus que le Boss, et bien sûr la Carte et la Boussole traditionnellement dessinées par Netgamer.

Bref, il devrait être terminé d'ici quelques jours ! Ensuite je m'attaquerai aux embûches que vous rencontrerez entre le donjon 5 et le donjon 6 (vous m'en direz des nouvelles) et puis au donjon 6, etc...

En plus avece les vacances qui approchent le jeu va pouvoir avancer rapidement. Ce qui peut nous laisser présager une date de sortie vers le mois de Mars (sous réserve...).
