---
date: '2003-11-28'
excerpt: 'Dimanche dernier a eu lieu un événement rare : la sortie d''un nouveau Zelda amateur en version complète ! Vous connaissez peut-être déjà...'
tags:
- solarus
title: 'The legend of Zelda : Attack of the Shadow'
---

Dimanche dernier a eu lieu un événement rare : la sortie d'un nouveau Zelda amateur en version complète !

Vous connaissez peut-être déjà Zelda : Attack of the Shadow, puisque le site officiel de ce projet fait partie de nos partenaires depuis longtemps. Il s'agit d'un Zelda amateur développé tout comme Zelda Solarus avec le logiciel RPG Maker. Doc, l'auteur de ce projet, reste modeste en affirmant que son jeu n'est pas aussi ambitieux que Zelda : Mystery of Solarus, mais à mon avis il n'a rien à lui envier et ça vaut vraiment le coup d'y jouer :)

Voici l'adresse du site officiel où vous pourrez télécharger Zelda : Attack of the Shadow : <http://zaots.free.fr>. Souhaitons une grande réussite à ce jeu qui va vite devenir une référence parmi les Zelda amateurs.
