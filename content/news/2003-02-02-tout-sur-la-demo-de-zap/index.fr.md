---
date: '2003-02-02'
excerpt: 'Pour ceux qui ne le sauraient pas encore, un petit rappel ne fait pas de mal. ZAP = Zelda : Advanced Project = le titre provisoire de notre prochaine...'
tags:
- solarus
title: Tout sur la démo de ZAP !
---

Pour ceux qui ne le sauraient pas encore, un petit rappel ne fait pas de mal. ZAP = Zelda : Advanced Project = le titre provisoire de notre prochaine création ! [Cliquez ici](http://www.zelda-solarus.com/jeux.php?jeu=zf) pour visiter la rubrique du jeu et voir en particulier tous les screenshots.

La démo contiendra en tout et pour tout un donjon, qui n'apparaîtra pas dans le jeu complet. En effet nous préférons dévoiler le moins de choses possibles sur la version finale.

Ce donjon a été conçu par pour être long, compliqué, et surtout anti-linéaire. C'est-à-dire que vous pourrez vous déplacer rapidement un peu partout dans le donjon, votre progression ne suivra pas un chemin unique préétabli. Niveau graphismes, nous utiliserons les mêmes que le Château d'Hyrule de Zelda 3 (rez-de-chaussée et cachots). Les décors seront beaucoups plus fouillés que dans Zelda Solarus, TGF oblige.

Pour ce qui est de la durée de vie, nous ne pouvons rien vous dire de précis car personne n'a encore pu jouer au jeu mis à part Netgamer en programmant. Mais on peut s'attendre au mieux car les énigmes sont nombreuses, et le donjon contient d'ailleurs trois Trésors au lieu de un ! Certaines énigmes reprendront par ailleurs des idées et même des décors de Zelda : Oracle of Ages & Seasons sur Game Boy Color.

Les abonnés à la [Newsletter](http://www.zelda-solarus.com/newsletter.php) savent tout ceci depuis le mois de novembre. Si ce n'est pas déjà fait, nous vous conseillons donc de vous y [abonner](http://www.zelda-solarus.com/newsletter.php) au plus vite si vous ne voulez plus rien rater, d'autant plus que les abonnés ont droit de temps en temps à certaines exclusivités, comme récemment des fonds d'écran de ZAP par exemple...

Voici maintenant des informations vraiment nouvelles sur l'avancement de la programmation de la démo. Toute la partie conception est terminée mis à part quelques petits détails. Le gros du travail qui reste à faire est donc la programmation. Et ça avance plutôt bien car les événements du donjon seront bientôt terminés. Il ne restera alors plus que le boss et le mini-boss à programmer ainsi que le système de sauvegarde et le menu.

Nous fixerons donc très bientôt la date de sortie de la démo et je pense que cela ne devrait pas vous décevoir ;-)

Pour ce qui est du jeu complet, vous en saurez plus dans une prochaine mise à jour !
