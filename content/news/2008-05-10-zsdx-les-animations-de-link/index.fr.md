---
date: '2008-05-10'
excerpt: "Le développement avance petit à petit ^^ Ces derniers jours j'ai travaillé sur certains mouvements de Link : pousser un mur, charger son épée..."
tags:
  - solarus
title: 'ZSDX : les animations de Link'
---

Le développement avance petit à petit ^^

Ces derniers jours j'ai travaillé sur certains mouvements de Link : pousser un mur, charger son épée et faire l'attaque tournoyante. Notez d'ailleurs que ces animations n'étaient pas présentes dans le Mystery of Solarus d'origine. Voici une image de l'attaque tournoyante (que je viens juste de terminer), aussi connue sous le nom d'attaque cyclone ^\_^ :

![](http://www.zelda-solarus.com/images/zsdx/spin_attack.png)

Le plus long pour faire ces animations, c'est de préparer chaque image de chaque animation au détail près. Il y avait plusieurs animations à faire : l'épée qui se charge et qui tourne autour de Link, mais aussi Link qui peut marcher pendant que l'épée se charge, le bouclier qui doit suivre, les étoiles qui apparaissent le long de l'épée lorsqu'elle se charge... Et tout cela pour chaque épée, chaque tunique et chaque bouclier :P. Bref, c'est un travail un peu fastidieux, mais heureusement c'est allé relativement vite puisque j'ai pu récupérer les images déjà faites pour Mercuris' Chest.

Maintenant, il n'y a plus qu'à faire des ennemis, pour que tout ceci serve à quelque chose :ninja:.

Sinon, nous travaillons également sur les nouvelles musiques, retouchées ou créées par George, ainsi que sur la conception des boss qui remplaceront ceux de Mystery of Solarus. De prochaines news y seront consacrées lorsque j'aurai plus de concret à vous montrer :).
