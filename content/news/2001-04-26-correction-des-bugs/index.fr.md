---
date: '2001-04-26'
excerpt: Tout d'abord, je tiens à vous signaler l'arrivée de Thomas, qui m'aide beaucoup à réaliser le jeu. Il a trouvé trois petits bugs dans le jeu :...
tags:
  - solarus
title: Correction des bugs
---

Tout d'abord, je tiens à vous signaler l'arrivée de [Thomas](http://www.zelda-solarus.com/mailto:net_gamer@hotmail.com), qui m'aide beaucoup à réaliser le jeu. Il a trouvé trois petits bugs dans le jeu : il était impossible de revenir dans le donjon 1 après l'avoir fini ; la pâtissière demandait toujours des pommes à Link et lui donnait une autre tondeuse ; et enfin Link ne pouvait jamais mourir en tombant dans le trou près de la cascade. Ce sont trois petits bugs, qui ont malheureusement survécu à nos nombreux tests.

Je viens donc de corriger ces bugs sur la démo. Attention : la démo elle-même n'a pas changé (elle va toujours jusqu'à la fin du premier donjon) mais ce sont simplement les bugs qui ont été corrigés. J'ai quand même fait deux petits changements : j'ai remplacé l'ancien écran-titre et l'ancien écran copyright par deux magnifiques écrans créés par [Thomas](http://www.zelda-solarus.com/mailto:net_gamer@hotmail.com).

Si vous avez déjà installé la première démo sur votre ordinateur, cela ne pose aucun problème : réinstallez la nouvelle démo dans le même dossier. Vous ne perdrez pas vos sauvegardes. Bien sûr, il est inutile dans ce cas de retélécharger les RTP.

- [La démo corrigée](http://www.zelda-solarus.com/download.php3?name=demo) - 1 Mo
- [Les RTP](http://www.zelda-solarus.com/download.php3?name=RTP) - 11 Mo (indispensables si vous ne les avez jamais installés)
- [Les polices françaises et l'icône](http://www.zelda-solarus.com/download.php3?name=solarus-polices_icone) - 6 Ko
