---
date: '2018-12-28'
excerpt: Solarus is finally available on Snap Store and Ubuntu Software Center.
tags:
- solarus
thumbnail: cover.jpg
title: Solarus on Snap Store
---

Solarus is finally available on **Snap Store** and **Ubuntu Software Center**. Installing Solarus has never been easier! Thanks to Alex Gleason.

### On Ubuntu

![Ubuntu Software Center icon](Ubuntu_Software_Center_icon_v3-01.png)

Search for **Solarus** in the Software Center and install it from here.

![Ubuntu Software Center](Screenshot-from-2018-12-28-10-15-37.png)

Click on **Install**.

![Solarus installation in Ubuntu Software Center](Screenshot-from-2018-12-28-10-16-13.png)

### On other Linux distros

![Get it from the Snap Store](snap-store-black.svg)

First, ensure you have **snapd** installed. If not, [install it with your package manager](https://docs.snapcraft.io/installing-snapd/6735). It should be something like:

```
sudo apt install snapd
```

Then, install **Solarus**.

```
sudo snap install solarus
```
