---
date: '2003-04-26'
excerpt: 1 an jour pour jour ! Que d'émotion d'avoir pu vous faire découvrir ce que nous voulions, l'histoire que nous avons voulu vous raconter, on espère...
tags:
- solarus
title: HAPPY BIRTHDAY SOLARUS !
---

1 an jour pour jour ! Que d'émotion d'avoir pu vous faire découvrir ce que nous voulions, l'histoire que nous avons voulu vous raconter, on espère qu'elle vous a plu et encore aujourd'hui. D'ailleurs on n'allait pas ne pas en parler, c'est pourquoi nous le faisons pendant tout un dossier :

[Dossier anniversaire ZS](http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=anniv)

Ce dossier contient aussi de nouvelles informations et images du jeu ZAP, comme promis.
