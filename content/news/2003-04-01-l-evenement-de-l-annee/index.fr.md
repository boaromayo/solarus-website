---
date: '2003-04-01'
excerpt: Fidèle fan de notre création ZAP, vous attendiez probablement ce moment depuis longtemps, nous avons enfin décidé de vous annoncer officiellement...
tags:
  - solarus
title: L'événement de l'année
---

Fidèle fan de notre création ZAP, vous attendiez probablement ce moment depuis longtemps, nous avons enfin décidé de vous annoncer **officiellement** la date de sortie de la démo du jeu "The Legend of Zelda : Advanced Project", la voici : **30 Septembre 2003**.

Ne vous inquiétez pas, c'est tout à fait compréhensible, malgré l'avancement plus que conséquent ces derniers jours, le logiciel TGF nous pose des problèmes. Autre raison : nous prévoyons aussi de traduire le jeu en plusieurs langues et le jeu sera donc dispo partout en même temps. Les grandes vacances vont donc beaucoup nous aider.

Pensez bien que ce délai assez allongé nous permettra de vous proposer un jeu de qualité.
