---
date: '2015-08-15'
excerpt: 'Zelda : Return of the Hylian is now available in English! The translation was imported from the original game that was already available in...'
tags:
  - solarus
title: Zelda Return of the Hylian in English
---

_Zelda : Return of the Hylian_ is now available in **English**!

The translation was imported from the original game that was already available in English. This is a beta translation, please report us any mistake. Enjoy!

- [Download](http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/)

Also, you can now decorate your desktop with a _Return of the Hylian_ wallpaper (1920x1080 px, ask for more sizes):

![Wallpaper (1920x1080 px)](roth_wallpaper_1920x1080-300x169.jpg)
