---
date: '2002-12-01'
excerpt: Bienvenue sur www.zelda-solarus.net !! Comme nous vous l'avions annoncé dans la newsletter, nous avons laissé tomber Consolemul qui faisait trop...
tags:
- solarus
title: Nouvelle adresse, nouveau design !
---

Bienvenue sur **www.zelda-solarus.net** !!

Comme nous vous l'avions annoncé dans la [newsletter](http://www.zelda-solarus.com/newsletter.php), nous avons laissé tomber Consolemul qui faisait trop de pub. Nous avons donc acheté un hébergement pro et surtout le nom de domaine www.zelda-solarus.net ! Finie la redirection en .fr.st. Mettez à jour vos favoris !

Pour l'occasion, Netgamer vous a conçu un sompteux design dans le style de Zelda : Kaze no Takuto ! On espère qu'il vous plaira ! L'interface est allégée. Le menu principal est désormais en haut. Le menu des jeux a été supprimé puisque la barre de navigation à l'aide des icônes suffit amplement. Enfin, le menu "A l'affiche" a également été allégé puisqu'il n'y a maintenant plus que deux liens.

Une bannière de pub sera bientôt affichée en haut à droite, à la place de celle de Zelda-Solarus, car il faut bien que nous trouvions de quoi financer ce nouvel hébergement... Mais rassurez-vous, cette fois-ci il n'y aura plus aucun popup, c'est promis !!!

Par ailleurs, vous pouvez désormais nous contacter via les adresses e-mail christopho@zelda-solarus.net et netgamer@zelda-solarus.net.

Toutes les pages ne sont peut-être pas encore complètement au point, mais nous nous occuperons des derniers petits problèmes dans les plus brefs délais. Par ailleurs nous apporterons très bientôt quelques petites améliorations à la navigation sur le site.

On attend vos réactions !
