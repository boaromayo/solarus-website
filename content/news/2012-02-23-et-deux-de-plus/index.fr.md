---
date: '2012-02-23'
excerpt: La soluce vidéo de Zelda Mystery of Solarus DX continue sur sa lancée !
tags:
  - solarus
title: Et deux de plus !
---

La soluce vidéo de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx) continue sur sa lancée ! Ce soir je viens d'enregistrer deux nouveaux épisodes avec Morwenn.

- [Épisode 7 : du niveau 3 au niveau 4](http://www.youtube.com/watch?v=abOn5rruZ3E)

- [Épisode 8 : niveau 4 - Palais de Beaumont](http://www.youtube.com/watch?v=PuBk5Z7JEJ8)

Nous en sommes à la fin du quatrième donjon. Bientôt la suite !
