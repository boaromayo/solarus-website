---
date: '2005-08-31'
excerpt: "Ca faisait longtemps qu'on vous les promettait ! Et bien les voici, les artworks officiels du jeu The Legend Of Zelda : Mercuris' Chest ! J'ai opté..."
tags:
  - solarus
title: Les artworks de Mercuris' Chest enfin publiés !
---

Ca faisait longtemps qu'on vous les promettait ! Et bien les voici, les artworks officiels du jeu The Legend Of Zelda : Mercuris' Chest ! J'ai opté pour un style assez cartoon entre les artworks de ALTTP et TWW. J'espère qu'ils vous plairont ! Vous pouvez y voir le boss présent dans la démo, ainsi que quelques ennemis, Link, Mercuris et un autre personnage très important du nom de Tannek. C'est un des premiers personnages qui donneront un coup de main à Link !

![](http://www.zelda-solarus.com/images/zf/artworks/link2_mini.jpg)

[Artworks de Zelda : Mercuris' Chest](http://www.zelda-solarus.com/jeux.php?jeu=zmc&zone=artworks)
