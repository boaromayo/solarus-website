---
date: '2013-06-25'
excerpt: And... Solarus 1.0.4 is out! Two crashes in ZSXD remained despite all my tests, and the customization screen of joypad commands was also...
tags:
  - solarus
title: Solarus 1.0.4 released, fixed crashes in ZSXD
---

And... Solarus 1.0.4 is out!

Two crashes in ZSXD remained despite all my tests, and the customization screen of joypad commands was also broken.

- [Download Solarus 1.0.4](http://www.solarus-games.org/downloads/download-solarus/ 'Download Solarus')
- [Download Zelda Mystery of Solarus DX 1.6.2](http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-dx/ 'Download ZSDX')
- [Download Zelda Mystery of Solarus XD 1.6.2](http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-xd/ 'Download ZSXD')

## Changes in Solarus 1.0.4

- Don't die if a script tries so show a missing string (#237).
- Don't die if a treasure has a variant unexistent in the item sprite.
- Fix customization of joypad commands.

## Changes in Zelda Mystery of Solarus DX 1.6.2

- Fix two line errors in English dialogs.

## Changes in Zelda Mystery of Solarus XD 1.6.2

- Fix a crash due to a missing text in the French options pause menu.
- Fix a crash due to an invalid treasure in a pot of crazy house 1F.
- Fix a typo in the French inventory pause menu.

Please continue to report any bugs. I expect more bugs to be found because this upgrade of both quest to Solarus 1.0 is really a heavy change for them.
