---
date: '2017-02-28'
excerpt: Le temple que vous voyez sur cette capture d'écran de Zelda Mercuris' Chest est bien caché. Link va devoir ruser pour traverser la forêt qui...
tags:
- solarus
title: Au cœur de la forêt
---

Le temple que vous voyez sur cette capture d'écran de Zelda Mercuris' Chest est bien caché. Link va devoir ruser pour traverser la forêt qui permet de l'atteindre.

![nature](nature-300x225.png)

Certains d'entre vous pourraient reconnaître cet endroit car je l'avais réalisé au cours d'un [live-streaming](https://www.youtube.com/watch?v=AWgEuNkE5Xs&index=3&list=PLzJ4jb-Y0ufxKu9TItbmyvjlJ3eSdsW0a). C'était en 2015 mais on n'avait pas encore publié beaucoup de captures d'écrans !
