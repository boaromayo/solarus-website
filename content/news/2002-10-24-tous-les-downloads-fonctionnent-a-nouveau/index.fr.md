---
date: '2002-10-24'
excerpt: Salut à tous ! Les downloads sont à nouveau disponibles. Je viens de les tester et ils fonctionnent tous ! Donc vous pouvez vous rendre sur la...
tags:
- solarus
title: Tous les downloads fonctionnent à nouveau !
---

Salut à tous !

Les downloads sont à nouveau disponibles. Je viens de les tester et ils fonctionnent tous ! Donc vous pouvez vous rendre sur la page [downloads](http://www.zelda-solarus.com/download.php) pour télécharger Zelda Solarus ou d'autres fichiers divers. Nous les avons changé de serveur pour que le téléchargement soit plus rapide.

Le site n'est pas encore totalement fonctionnel (certaines images manquent) mais nous faisons tout notre possible pour y remédier rapidement. Merci de votre compréhension ;)
