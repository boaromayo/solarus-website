---
date: '2002-11-04'
excerpt: Après deux semaines pendant lesquelles DaN nous a hébergés provisoirement (merci à lui !), nous voilà enfin sur Consolemul. Cet hébergement est...
tags:
- solarus
title: Déménagement terminé !
---

Après deux semaines pendant lesquelles DaN nous a hébergés provisoirement (merci à lui !), nous voilà enfin sur Consolemul. Cet hébergement est donc définitif. Merci à Sir Jaguar d'avoir accepté notre demande ;)

Je le répète une fois de plus, l'adresse du site ne change pas : www.zelda-solarus.fr.st.

Tous les liens doivent fonctionner. Si toutefois vous trouvez un bug, signalez-le moi.
