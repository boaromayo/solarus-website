---
date: '2002-12-15'
excerpt: Salut à tous ! Il était temps que nous changions le design des forums ! Nous les avons déménagés sur le nouveau serveur, mais le mieux est...
tags:
- solarus
title: Relookage des forums
---

Salut à tous !

Il était temps que nous changions le design des forums ! Nous les avons déménagés sur le nouveau serveur, mais le mieux est que Netgamer les a relookés aux couleurs du nouveau design du site. Et comme d'habitude c'est du grand art !

Nous en avons profité pour refaire les images qui avaient été perdues à cause de la fermeture de notre ancien compte sur Free. Les images des grades sont donc à nouveau présentes ! Et la page d'accueil des forums a désormais enfin un beau titre ;-)

Tous les utilisateurs et les messages ont bien sûr été conservés, sauf les topics qui étaient sans réponse depuis plus d'un mois.

Voilà, on espère que ce relookage complet redonnera un peu plus de vie aux forums en attendant la sortie de la démo de ZAP !

[Visiter les forums](http://www.zelda-solarus.com/forums)
