---
date: '2001-07-05'
excerpt: "Vous l'avez vu sur un des screenshots de la galerie, les Boss sont bien plus impressionnants que dans la démo. Revoici la fameuse image : Je vais..."
tags:
  - solarus
title: Tout ce que vous voulez savoir à propos des Boss !
---

Vous l'avez vu sur un des screenshots de la galerie, les Boss sont bien plus impressionnants que dans la démo. Revoici la fameuse image :

![](solarus-ecran22.png)

Je vais peut-être vous décevoir, mais il est impossible d'animer le Boss sous RPG Maker 2000. Le dragon est en fait une image statique. Heureusement pour lui (et malheureusement pour Link qui a l'air bien mal en point !), le Boss est épaulé de ses sbires les lézards. Il vous faudra donc vous débarasser des petites bêtes pour pouvoir vous attaquer au Boss proprement dit. Le problème est que les lézards réaparaissent ! Et oui, ça aurait été trop facile...

Une dernière chose importante : je vous conseille de bien vérifier qu'il sera vraiment mort lorsque vous l'aurez tué...

N'hésitez pas à laisser tous vos commentaires sur le [forum](http://www.zelda-solarus.com/forum.php3) !
