---
date: '2017-03-29'
excerpt: Aujourd'hui on vous dévoile un tout petit peu plus de détails sur le désert de Zelda Mercuris' Chest, qui est pour ne rien vous cacher une de mes...
tags:
- solarus
title: Le désert
---

Aujourd'hui on vous dévoile un tout petit peu plus de détails sur le désert de Zelda Mercuris' Chest, qui est pour ne rien vous cacher une de mes maps préférées.

![desert](desert-300x225.png)

Le projet avance de plus en plus, on travaille en équipe sur plusieurs maps et donjons en parallèle. À la semaine prochaine !
