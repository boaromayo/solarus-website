---
date: '2002-05-11'
excerpt: Nous avons relooké le forum aux couleurs du site ! Nous sommes en train de terminer les dernières icônes pour que ce soit parfait ! Puis, comme...
tags:
- solarus
title: Nouvelle peau pour le forum
---

Nous avons relooké le forum aux couleurs du site ! Nous sommes en train de terminer les dernières icônes pour que ce soit parfait !

Puis, comme certaines personnes n'aiment pas vraiment les couleurs, nous allons préparer plusieurs thèmes pour le forum, vous choisirez celui qui vous plaît !

[SOLARUS FORUMS](http://www.zsforums.fr.st)
