---
date: '2010-02-10'
excerpt: 'Welcome to the development blog of Solarus, the open-source Zelda-like 2D game engine. Solarus is the engine developed for the game Zelda: Mystery of...'
tags:
- solarus
title: Welcome
---

Welcome to the development blog of Solarus, the open-source Zelda-like 2D game engine. Solarus is the engine developed for the game Zelda: Mystery of Solarus DX. This blog has several purposes:

- Provide access to the source code of the engine (Solarus) and the game(s), including the development version
- Give news about the development and some technical information about how the engine works
- Encourage contributions for translations, ports to new systems, development tools, and new quests

Some pages are already available and others will come soon. For now, you can get the [source code](http://www.solarus-engine.org/source-code/) and see the [Zelda Mystery of Solarus DX](http://www.solarus-engine.org/zelda-mystery-of-solarus-dx/) section. You can also [contact me](http://www.solarus-engine.org/contact/) for any suggestion.
