---
date: '2020-02-09'
excerpt: The website now features walkthroughs, and Mystery of Solarus DX is the first to come.
tags:
  - website
thumbnail: cover.jpg
title: Game walkthroughs on the website
---

For all those who are lost and can't beat the Solarus quests we feature on this website, we are glad to announce that the website now allows the addition of walkthroughs for the games.

The first walkthrough to be online is obviously [the one for _Mystery of Solarus DX_](/walkthroughs/the-legend-of-zelda-mystery-of-solarus-dx). Renkineko worked on this thorough and complete walkthrough a few years ago, and he made it in both English and French. You'll find screenshots, dungeon maps, heart pieces locations and very precise instructions to beat the game to 100%. So shout out to him for this huge work!
