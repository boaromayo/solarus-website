---
date: '2017-01-30'
excerpt: Aujourd'hui il est temps de vous présenter un autre donjon. Réalisé par Metallizer, il promet d'être original et mémorable. On vous en...
tags:
- solarus
title: Un donjon et un coffre
---

Aujourd'hui il est temps de vous présenter un autre donjon. Réalisé par Metallizer, il promet d'être original et mémorable. On vous en dévoilera un peu plus dans les prochaines semaines.

![lighthouse_chest](lighthouse_chest-300x225.png)

Notez que Newlink nous propose ici un nouveau graphisme pour le coffre :)
