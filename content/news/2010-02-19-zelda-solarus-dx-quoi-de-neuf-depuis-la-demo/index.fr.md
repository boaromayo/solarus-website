---
date: '2010-02-19'
excerpt: Il y a deux mois (et un jour) sortait la démo de notre projet Zelda Mystery of Solarus DX.
tags:
  - solarus
title: 'Zelda Solarus DX : quoi de neuf depuis la démo ?'
---

Il y a deux mois (et un jour) sortait la [démo](http://www.zelda-solarus.com/jeu-zsdx-demo) de notre projet Zelda Mystery of Solarus DX.
Je fais un point aujourd'hui sur les principales avancées qui ont été réalisées depuis ce moment, essentiellement du point de vue du moteur et des questions techniques. Les prochaines mises à jour parleront de la quête avec notamment des images du deuxième donjon?

## Moteur de jeu

- Le choix de la langue se fait maintenant lors du premier lancement du jeu. Il n'y aura donc plus qu'une seule version à télécharger, contenant toutes les langues. Je prévois de faire un écran d'options proposant de changer la langue sélectionnée initialement. Cet écran d'option permettra également d'effectuer d'autres réglages comme le volume de la musique et des effets sonores ou encore la résolution du jeu.

- Le menu principal du jeu (écran-titre puis sélection des sauvegardes) peut maintenant être contrôlé au joypad si vous le souhaitez.

- L'arc est programmé et entièrement fonctionnel. Les Bottes de Pégase sont en cours de développement.

## Corrections diverses

- Nombreux [bugs](http://forums.zelda-solarus.com/index.php/board,62.0.html) corrigés. Merci à tous ceux qui nous en signalent régulièrement.

- Amélioration de divers éléments où l'on pouvait faire des reproches dans la démo : l'intro a été un peu raccourcie, la difficulté de certaines énigmes ou certains combats a été ajustée.

- Beaucoup de travail a été fait sur le code source pour le rendre plus propre, plus uniformisé et plus évolutif. Le moteur utilise différentes bibliothèques (comme SDL, OpenAL), mais seule une petite partie du code source bas niveau du moteur fait appel à leurs fonctions. L'essentiel du code s'appuie sur cette partie bas niveau et fait abstraction des bibliothèques utilisées. L'intérêt de ceci étant que si on veut remplacer une bibliothèque par une autre (par exemple pour porter le jeu vers un système spécial B)), il n'y a qu'une petite partie du code à changer.

## Distribution des futurs versions publiques

- Le moteur de jeu est désormais bien séparé de la quête. Cela pourra laisser la possibilité de créer d'autres quêtes utilisant le même moteur. Le moteur est nommé "Solarus" et son [code source](http://www.solarus-engine.org/source-code) C++ est distribué sous licence GNU GPL. La quête "Zelda Mystery of Solarus" n'a pas de licence à proprement parler étant donné qu'elle utilise des graphismes et musiques de Nintendo.

- Le système de compilation et d'installation a été entièrement revu pour permettre de compiler et distribuer plus facilement le moteur, la ou les quête(s) ou le tout.

- J'ai ouvert un [blog de développement de Solarus](http://www.solarus-engine.org). Ce blog (en anglais) est dédié aux questions techniques du développement du moteur. Il n'est donc pas destiné au grand public mais intéressera plutôt les développeurs et les personnes souhaitant contribuer au projet. Il ne contient pas encore énormément de choses, mais il y a déjà l'accès au code source de la version de développement et des informations techniques sur la compilation et l'installation. :)
