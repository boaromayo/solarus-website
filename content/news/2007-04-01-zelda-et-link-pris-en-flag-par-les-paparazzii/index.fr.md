---
date: '2007-04-01'
excerpt: Cette nouvelle ne risque pas d'en surprendre plus d'un puisqu'en fait, beaucoup de rumeur courraient à ce sujet. En effet, il semblerait enfin que...
tags:
  - solarus
title: Zelda et Link pris en flag par les paparazzii
---

Cette nouvelle ne risque pas d'en surprendre plus d'un puisqu'en fait, beaucoup de rumeur courraient à ce sujet. En effet, il semblerait enfin que leur amour soit mis au grand jour car ils ont été pris en flagrant délit à se promener main dans la main dans la plaine d'Hyrule. Cela pourrait expliquer par conséquent pourquoi Link et Tétra sont si proches...

![](http://hommeenmousse.free.fr/wp-content/zelda_01.jpg)

Et dire qu'ils font les ignorants sur cette photo alors qu'ils viennent d'être aperçu à fleureter ensemble...
Je suspecte un lien de parenté entre nos deux héros de TWW... Pas vous ?
