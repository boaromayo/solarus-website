---
date: '2011-08-08'
excerpt: 'Yes, the title is correct: Zelda Mystery of Solarus XD, not DX! DX is in progress too, but this is not what I''m talking about today! Zelda Mystery...'
tags:
- solarus
title: 'Coming soon: Zelda Mystery of Solarus XD'
---

Yes, the title is correct: Zelda Mystery of Solarus XD, not DX! DX is in progress too, but this is not what I'm talking about today!

Zelda Mystery of Solarus XD (or ZSXD for short) is one of our creations. It's a small parodic game that we released on April 1st, 2011. I did not talk about it earlier on this blog because the game was only available in French. However, people from the [Caanoo](http://en.wikipedia.org/wiki/Caanoo) French-speaking community are currently working on a Caanoo port of this parodic game ZSXD. And they decided to translate it in English. We helped them for the translation, and it's almost done! It still needs some final verifications.

Once it's done, we will be able to make a release of ZSXD in English! Though it's a parodic mini-game (initially a big April 1st joke), it's a real, full game with two huge dungeons and 5-10 hours of playing.

Here are some screenshots:

![](yoda.png "Yoda")
![](long_text.png "Long text")
![](creeper.png "Creeper!")
![](strike.png "Strike")
![](cannonballs.png "Cannonballs")Stay tuned ;)
