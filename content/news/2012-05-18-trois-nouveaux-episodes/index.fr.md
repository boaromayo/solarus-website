---
date: '2012-05-18'
excerpt: 'La fin du jeu approche ! Nous continuons la solution en vidéo avec trois nouveaux épisodes.'
tags:
  - solarus
title: Trois nouveaux épisodes
---

La fin du jeu approche !

Nous continuons la solution en vidéo avec trois nouveaux épisodes : le donjon secret, du niveau 6 au niveau 7, et le niveau 7. Merci à Binbin et BenObiWan pour les commentaires :)

- [Épisode 13 : Donjon secret](http://www.youtube.com/watch?v=0mQrcTSgTt8)

- [Épisode 14 : Du niveau 6 au niveau 7](http://www.youtube.com/watch?v=VhKdF1L7Vak)

- [Épisode 15 : Temple du Cristal](http://www.youtube.com/watch?v=Q1hjSJMHr0Y)

Enjoy ! ^\_^
