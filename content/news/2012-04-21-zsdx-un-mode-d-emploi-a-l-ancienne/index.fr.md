---
date: '2012-04-21'
excerpt: Afin de pousser la nostalgie jusqu'au bout, voici un mode d'emploi comme on en faisait dans les années 90 ! C'est fort dommage que depuis quelques...
tags:
  - solarus
title: "ZSDX : un mode d'emploi à l'ancienne"
---

Afin de pousser la nostalgie jusqu'au bout, voici un mode d'emploi comme on en faisait dans les années 90 ! C'est fort dommage que depuis quelques années, les modes d'emploi de jeu vidéo ne sont plus ce qu'ils étaient, c'est à dire généreux en informations, en illustrations, décrivant le jeu dans ses moindres détails... Je ne sais pas pour vous, mais personnellement, j'adorais lire ces petits livrets ! Mais avec la dématérialisation des jeux vidéo et les économies faites en n'imprimant pas de mode d'emploi, déballer un jeu a perdu un peu de sa saveur !
Le mode d'emploi est donc réalisé dans la grande tradition des modes d'emploi de Zelda. Si vous ne vous souvenez pas à quoi ressemblait celui de [A Link To The Past](http://www.zelda-solarus.com/jeu-z3), voici un rappel :

![](http://www.zelda-solarus.com/images/uploads/alttp_manuel_1.jpg)

![](http://www.zelda-solarus.com/images/uploads/alttp_manuel_2.jpg)

Et voici à quoi ressemble maintenant le manuel de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx), hommage à celui de A Link To The Past :

![](http://www.zelda-solarus.com/images/uploads/zsdx_manuel_1.jpg)

![](http://www.zelda-solarus.com/images/uploads/zsdx_manuel_2.jpg)

J'en ai profité pour refaire tous les logos en vectoriel, d'ailleurs si vous observez bien, le "Z" de "Zelda" est étiré à la façon des titres des Zelda old-school (ALTTP, Link's Awakening) mais façon cell-shading moderne comme depuis Wind Waker. :) Et puis j'ai également refait plein de dessins pour illustrer le mode d'emploi, dans un style mélange de BD franco-belge et de manga, en essayant de donner un style original. Le mode d'emploi fait ainsi 38 pages ! Pas de panique pour ceux qui ne veulent pas le lire, car un mode d'emploi express a aussi été fait, avec les commandes de base expliquées en une page.

![](http://www.zelda-solarus.com/images/uploads/zelda_mos_dx_logo_vector_petit.png)

Si jamais vous essayez d'imprimer le mode d'emploi, vous verrez que ça ne colle pas aux proportions d'une feuille A4... c'est normal ! C'est en fait les proportions d'un manuel de Super Nintendo ! ;)

![](http://www.zelda-solarus.com/images/uploads/link_brandit_epee_petit.jpg)

D'ailleurs à ce propos, la fausse boite SNES a également été refaite, et si vous l'imprimez sur une feuille A3, et que vous suivez les pliages, elle aura exactement la taille d'une vraie boite de jeu SNES !

[![](http://www.zelda-solarus.com/images/uploads/boite_front_300px.jpg)](http://www.zelda-solarus.com/zsdx/boite.pdf)

- [Télécharger le mode d'emploi](http://www.zelda-solarus.com/zsdx/mode_emploi/zsdx_mode_emploi_fr.pdf)

- [Télécharger le mode d'emploi express](http://www.zelda-solarus.com/zsdx/mode_emploi/zsdx_mode_emploi_express_fr.pdf)

Bonne lecture !
