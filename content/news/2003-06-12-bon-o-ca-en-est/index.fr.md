---
date: '2003-06-12'
excerpt: 'Nous travaillons actuellement près de dix heures par jour sur la démo de Zelda : Advanced Project (titre provisoire). Il reste encore beaucoup de...'
tags:
- solarus
title: Bon, où ça en est ?
---

Nous travaillons actuellement près de dix heures par jour sur la démo de Zelda : Advanced Project (titre provisoire). Il reste encore beaucoup de travail mais le plus dur est fait. La démo devrait sortir cet été. La prochaine news à propos de ZAP risque bien d'être la date de sortie !
