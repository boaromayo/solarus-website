---
date: '2005-08-31'
excerpt: Nous avons totalement refait le système des mises à jour du site. Les mises à jour peuvent maintenant être effectuées de manière plus simple...
tags:
  - solarus
title: Nouveau système de news et de commentaires !
---

Nous avons totalement refait le système des mises à jour du site. Les mises à jour peuvent maintenant être effectuées de manière plus simple qu'avant par les différents membres de l'équipe. La grande nouveauté pour vous chers visiteurs, c'est la prise en charge des smileys ! :D

Mais le principal changement concerne surtout les commentaires. Désormais, les commentaires se font sur le forum ! Un nouveau forum "Commentaires des mises à jour" à été créé à cet effet et chaque mise à jour possède un topic, dans lequel vous pouvez poster vos commentaires. Comme sur le reste du forum, vous devez être inscrit pour pouvoir poster, afin d'éviter les abus. L'avantage, c'est que vous n'aurez plus besoin d'attendre que vos commentaires soient validés avant qu'ils apparaissent. Et qui dit commentaires sur le forum dit aussi commentaires avec smileys, utilisation du BBCode et tout ce qu'offre le forum ^\_^

[Forums Zelda Solarus](http://forums.zelda-solarus.com)
