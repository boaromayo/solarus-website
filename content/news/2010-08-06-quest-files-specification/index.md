---
date: '2010-08-06'
excerpt: The project is making good progress these days, though it was hard for me during the last few months to find much time to work on Solarus, for...
tags:
- solarus
title: Quest files specification
---

The project is making good progress these days, though it was hard for me during the last few months to find much time to work on Solarus, for professional reasons (I am finishing my phd thesis) and personal ones (I just got married!).

If you are interested in how a quest is built, I just published some detailed documentation about the different [data files that compose a quest](http://www.solarus-engine.org/doc/quest.html). This documentation specifies the format of each file:

- [Maps](http://www.solarus-engine.org/doc/map_syntax.html)
- [Tilesets](http://www.solarus-engine.org/doc/tileset_syntax.html)
- [Sprites](http://www.solarus-engine.org/doc/sprite_syntax.html)
- [Dialog and texts](http://www.solarus-engine.org/doc/dialog_syntax.html)
- [Dungeons](http://www.solarus-engine.org/doc/dungeon_syntax.html)
- [Lua scripting API](http://www.solarus-engine.org/doc/lua_api_specification.html)
