---
date: '2020-04-01'
excerpt: Supportez Solarus en achetant des goodies officiels sur Spreadshirt.
tags:
  - solarus
  - solarus labs
thumbnail: cover.jpg
title: Vous pouvez désormais acheter des goodies Solarus !
---

Avez-vous déjà rêvé de portez des sous-vêtements Solarus ?

Eh bien... c'est désormais possible. Solarus a désormais une ligne de vêtements incluant des T-shirts, des hauts, des sweats à capuches, des casquettes et mêmes des sacs !

![tshirt](spreadshirt-shop.jpg)

Tous les bénéfices iront directement à l'association à but non-lucratif [Solarus Labs](/fr/about/nonprofit-organization) que nous avons créée en Janvier 2020. L'argent sera utilisé pour aider Solarus, en payant les coûts d'hébergement ou en achetant du matériel sur lequel vous voudriez voir Solarus être porté, par exemple. Nous envisageons également d'organiser des évènements comme des concours ou des rencontres, si les sommes nous le permettent.

Avouez que vous auriez l'air bien plus cool avec une casquette à l'effigie de Solarus Quest Editor, non ?

Ah. Et _oui_, vous pouvez réellement acheter un boxer Solarus.

![boxer](boxer.jpg)

Toutes ces merveilles sont disponibles dans la [boutique Spreadshirt](https://shop.spreadshirt.fr/solarus-labs). Amusez-vous bien à déambuler dans les rayons de notre boutique !

![banner](spreadshirt-banner.png)
