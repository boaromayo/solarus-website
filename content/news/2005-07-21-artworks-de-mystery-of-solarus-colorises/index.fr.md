---
date: '2005-07-21'
excerpt: Forza a eu la superbe idée de coloriser les artworks officiels de Mystery of Solarus ! Leur qualité est ainsi nettement supérieure, et le...
tags:
- solarus
title: Artworks de Mystery of Solarus colorisés
---

Forza a eu la superbe idée de coloriser les artworks officiels de Mystery of Solarus !

Leur qualité est ainsi nettement supérieure, et le plaisir de les regarder plus grand ! Pour fêter cela comme il se doit, je vous présente une nouvelle version de Link, plus travaillée, plus manga et plus expressive ! Voici donc ce nouveau Link :

![](link2_mini.jpg)

Les autres artworks colorisés illustrent le [mode d'emploi](http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=notice) de Zelda : Mystery of Solarus.
