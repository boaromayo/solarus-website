---
date: '2012-02-22'
excerpt: Deux nouveaux épisodes de la soluce vidéo de Zelda Mystery of Solarus DX sont disponibles depuis.
tags:
  - solarus
title: Encore deux nouveaux épisodes
---

Deux nouveaux épisodes de la soluce vidéo de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx) sont disponibles depuis ce matin !

- [Épisode 5 : du niveau 2 au niveau 3](http://www.youtube.com/watch?v=b5nG2ySbX1w)

- [Épisode 6 : niveau 3 - Antre de Maître Arbror](http://www.youtube.com/watch?v=nr_KzjsMw4o)

Et c'est avec Thyb que j'ai eu le plaisir de commenter ces deux épisodes. La soluce va donc maintenant jusqu'à la fin du troisième donjon. Le tournage de la suite est prévu pour jeudi soir ;)
