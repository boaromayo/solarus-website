---
date: '2004-01-09'
excerpt: Enfin ! Après des mois de problèmes d'hébergement et de nom de domaine, on y est arrivé ! Comme vous avez pu le constater, nous avons un...
tags:
- solarus
title: 'Nouvelle adresse : www.zelda-solarus.com'
---

Enfin ! Après des mois de problèmes d'hébergement et de nom de domaine, on y est arrivé !

Comme vous avez pu le constater, nous avons un nouveau nom de domaine : **www.zelda-solarus.com**. L'ancien est à oublier :)

Mais nous avons surtout changé d'hébergeur, ce que nous vous promettions depuis le mois de novembre. Notre nouvel hébergeur nous laisse plus de liberté en terme de requêtes envoyées au serveur, et nous avons donc pu remettre toutes les images, les forums et les téléchargements sur ce serveur au lieu d'utiliser un compte free très lent et un compte consomemul rapide mais avec de la pub. Maintenant tout est sur le même serveur et on voit déjà la différence de rapidité. Et bien entendu il n'y a plus de pub ni de popups !
