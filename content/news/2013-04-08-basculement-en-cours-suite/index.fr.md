---
date: '2013-04-08'
excerpt: Bonsoir à toutes et à tous :) C'est avec plaisir que je poste ce soir ma première news sur le nouveau site. Je tiens dans un premier temps à...
tags:
- solarus
title: Basculement en cours (suite)
---

Bonsoir à toutes et à tous :)

C'est avec plaisir que je poste ce soir ma première news sur le nouveau site. Je tiens dans un premier temps à remercier Christopho pour la confiance qu'il m'a accordé pour le développement du site. C'était un vrai plaisir de le réaliser.
Merci aussi à toutes les personnes qui de près ou de loin m'ont aidé à élaborer ce projet.
J'espère que ce site vous plait et qu'il permettra de vous apporter pleins de choses :)
J'ai passé ces derniers jours à corriger pas mal de bugs en tout genre et de nouvelles améliorations devraient voir le jour ces prochaines semaines et ces prochains mois.

En attendant, le basculement du contenu continue encore et encore !
En effet, [la soluce de A Link To The Past](http://www.zelda-solarus.com/zs/article/zalttp-soluce/) est à présent complètement publiée. Merci encore Renkineko pour son travail remarquable.
De plus, il a aussi commencé à déplacer [la soluce de The Adventure Of Link](http://www.zelda-solarus.com/zs/article/ztaol-soluce/), illustrée elle aussi.

De son coté, Valoo a presque terminé de migrer [la solution de Ocarina Of Time (Master Quest)](http://www.zelda-solarus.com/zs/article/zoot-soluce/).

Enfin, Morwenn est en train de rédiger la fin de [la solution de Link's Awakening](http://www.zelda-solarus.com/zs/article/zla-soluce/). [Le temple du masque (partie 12)](http://www.zelda-solarus.com/zs/soluce/zla-soluce-chapitre-12-temple-du-masque-niveau-6/) est déjà en ligne !

*Pendant ce temps, le printemps s'installe tout doucement et  le peuple Mojo attend l'E3 avec impatience ...*
