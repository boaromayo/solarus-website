---
date: '2003-11-19'
excerpt: Nous allons changer d'hébergeur à la fin de cette semaine. Pendant le déménagement, le site sera malheureusement indisponible mais ne vous...
tags:
- solarus
title: Déménagement imminent
---

Nous allons changer d'hébergeur à la fin de cette semaine. Pendant le déménagement, le site sera malheureusement indisponible mais ne vous inquiétez pas, il reviendra très vite.

Les images, qui bugguent régulièrement, n'auront plus de problème et le site sera globalement plus rapide.

Nous déménagerons également les forums sur le nouveau serveur et il n'y aura donc plus de pub !

Bref, merci de votre patience et à très bientôt sur www.zelda-solarus.net :)
