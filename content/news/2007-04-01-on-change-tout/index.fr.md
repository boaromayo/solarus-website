---
date: '2007-04-01'
excerpt: Le recrutement de Marco n'était que la première étape d'un processus destiné à redonner vie au site et au forum. D'autres mesures vont être...
tags:
- solarus
title: On change tout !
---

Le recrutement de Marco n'était que la première étape d'un processus destiné à redonner vie au site et au forum. D'autres mesures vont être prises, en commençant par un nouveau design.

Comme vous le voyez, nous voulons rendre le site plus vivant en lui donnant une orientation plutôt "Blog". Les sites classiques, c'est dépassé. Les blogs sont l'avenir du web ! Cela permettra de rendre Zelda-Solarus à la fois plus joli, plus attrayant et surtout plus interactif.

Des changements vont également être apportés au forum très prochainement. Nous espérons que tout cela vous plaira et redonnera du dynamisme à Zelda-Solarus :)
