---
date: '2017-10-11'
excerpt: As you may have noticed, Solarus recently got some spotlight thanks to several websites. PC Gamer made a interview of Christopho and talked about...
tags:
- solarus
title: Solarus Press Kit
---

As you may have noticed, Solarus recently got some spotlight thanks to several websites. PC Gamer made a interview of Christopho and talked about Solarus in a [really good article](http://www.pcgamer.com/zeldas-most-dedicated-fan-game-developers-built-an-engine-anyone-can-use/).

Since several websites may want to talk about Solarus, we made a press kit so it's easy for them to gather logos, Editor screenshots and more. It's [here](http://www.solarus-games.org/engine/press/).
