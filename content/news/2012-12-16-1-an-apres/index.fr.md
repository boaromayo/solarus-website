---
date: '2012-12-16'
excerpt: Joyeux anniversaire à Zelda Mystery of Solarus DX qui fête ses un an ce soir ! Il y a un an, je...
tags:
  - solarus
title: 1 an après
---

Joyeux anniversaire à [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx) qui fête ses un an ce soir ! Il y a un an, je venais d'enchaîner deux nuits blanches, aujourd'hui c'est nettement plus calme ^\_^.

Mais il va bientôt y avoir du nouveau sur Zelda Solarus. D'abord, nous préparons une nouvelle version du site plus moderne et dont le contenu sera je l'espère mis à jour plus régulièrement. Nous recherchons d'ailleurs des personnes pour faire des news sur l'actualité de Zelda, des soluces et autres. N'hésitez pas à me contacter :).

Et surtout, ça avance en coulisses du côté des jeux amateurs. Depuis la sortie de Zelda Mystery of Solarus DX il y a un an, j'ai beaucoup travaillé sur le moteur de jeu afin de le rendre plus général. J'en ai un peu parlé sur le [blog de développement](http://www.solarus-games.org) et pas encore ici. Le but est simple : pouvoir créer des nouveaux jeux en réutilisant le moteur, mais en ayant la possibilité de changer de nombreux éléments comme l'écran-titre ou le menu de pause. Si vous avez joué à Zelda Mystery of Solarus DX et XD, les deux jeux qui utilisent le moteur à l'heure actuelle, vous avez sans doute remarqué que l'écran-titre, le menu de pause et les icônes qui s'affichent à l'écran sont exactement les mêmes. Ce qui est un peu restrictif si vous voulez faire votre propre Zelda-like. Beaucoup de choses étaient déjà personnalisables (comme les ennemis et les objets de l'inventaire) mais on peut faire mieux.

Bref, 2011 a été l'année où j'ai stoppé les évolutions du moteur pour pouvoir finir le jeu, et 2012 a été l'année où j'ai continué le moteur. Du coup, 2013 sera l'année pour faire des nouveaux jeux ! Lesquels ? C'est trop tôt pour le dire. Ce n'est pas encore clair même pour moi...

Fin décembre, nous organisons un week-end intensif de création de jeux amateurs. Je pense que des choses passionnantes vont démarrer là. Mais je ne sais pas encore quoi. J'ai à peu près 6 ou 7 idées de jeux que je rêverais de faire. Il va donc falloir choisir, et nous y verrons plus clair à ce moment-là :).
