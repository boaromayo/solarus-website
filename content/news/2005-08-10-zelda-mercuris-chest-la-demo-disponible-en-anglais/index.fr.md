---
date: '2005-08-10'
excerpt: Je suis heureux de vous annoncer que comme promis, la démo est maintenant disponible en version anglaise ! La traduction a été...
tags:
  - solarus
title: "Zelda Mercuris' Chest : la démo disponible en anglais"
---

Je suis heureux de vous annoncer que comme promis, la démo est maintenant disponible en version anglaise ! La traduction a été faite par Geomaster. Tout le jeu est donc traduit en Anglais ainsi que le mode d'emploi.

![](http://www.zelda-solarus.com/images/zf/notice/en/01.png)

- [Zelda : Mercuris' Chest démo version anglaise](jeux.php?jeu=zmc&zone=english)
- [Mode d'emploi en Anglais](jeux.php?jeu=zmc&zone=notice&langue=en)

Nous espérons que cette traduction en Anglais permettra à Zelda Solarus de se faire mieux connaître dans le monde :)
