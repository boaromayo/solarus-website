---
date: '2020-02-20'
excerpt: The project is now backed by a nonprofit-organization named Solarus Labs.
tags:
- solarus
- solarus labs
thumbnail: cover.jpg
title: Solarus Labs, a nonprofit organization
---

Until now, Solarus was not backed by a legally existing organization. We're happy to announce this is now over!

![Solarus Labs logo](solarus-labs-logo.png)

We created **Solarus Labs**, a French non-profit organization, based in France (because Solarus' creator Christopho is French). You can find more information in the [organization's page](/en/about/nonprofit-organization).

It does not change anything from a non-legal point of view. The project is still free and open-source and will be forever, and non-French people can still contribute to it.

The only difference is that the project has now a legal entity to receive donations. Obviously, and as before, all the donations will be reinvested into the project.
