---
date: '2002-08-05'
excerpt: 'Ca faisait longtemps que vous nous la réclamiez, et la voilà ! La soluce complète de Zelda : Mystery of Solarus est enfin en ligne, et elle va...'
tags:
- solarus
title: Enfin la soluce !
---

Ca faisait longtemps que vous nous la réclamiez, et la voilà ! La soluce complète de Zelda : Mystery of Solarus est enfin en ligne, et elle va jusqu'au quatrième donjon. Mais attention, elle est à consulter avec modération si vous ne voulez pas finir le jeu trop vite et trop facilement !

[Soluce de Zelda Solarus](http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=soluce)
