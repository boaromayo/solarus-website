---
date: '2002-04-25'
excerpt: Ca y est ! Ca y est ! Et cette fois, pas de faux départ ! Je pense que vous devez êtes surpris de ce nouveau design, même si vous vous y...
tags:
- solarus
title: Le jeu !
---

Ca y est !

Ca y est ! Et cette fois, pas de faux départ !

Je pense que vous devez êtes surpris de ce nouveau design, même si vous vous y attendiez peut-être ! Comme vous pouvez le constater le site s'agrandit, puisqu'en plus de Zelda Solarus, il va aussi parler de tous les autres Zelda, les vrais ! Avec des soluces complètes, des images, des musiques... Laissez-nous juste le temps de les faire !

Vous pouvez lire l'[édito](http://www.zelda-solarus.com/site.php3?page=edito) de Netgamer, qui vous présentera le jeu et le site. Autre chose à ne pas manquer : la soluce complète de [Zelda : Majora's Mask](http://www.zelda-solarus.com/jeux.php3?jeu=z6&zone=soluce) est déjà en ligne, même si elle ne va pas encore jusqu'à la fin du jeu). D'autres pages sont également créées, vous pouvez parcourir le menu de gauche. Le site se remplira au fur et à mesure avec le temps.

Mais venons-en à ce que vous attendez depuis des mois, c'est-à-dire [Zelda : Mystery of Solarus](http://www.zelda-solarus.com/jeux.php3?jeu=zs&zone=download), le jeu, en version complète, qui est disponible !

Et puis pour finir, n'oubliez pas de visiter les nouveaux forums, beaucoup plus performant que l'ancien (qui avait cependant le mérite d'être programmé par mes soins...). Retenez cette adresse : <http://www.zsforums.fr.st>. Trois forums sont installés : un forum rien que pour ZS, à consulter si vous êtes bloqué ou si vous voulez tout simplement parler du jeu, un forum sur tous les Zelda et enfin un forum libre.

Voilà, je crois que tout est dit. Je vous souhaite une bonne quête. Si, si, c'est sincère.
