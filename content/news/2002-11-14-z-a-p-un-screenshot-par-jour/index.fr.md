---
date: '2002-11-14'
excerpt: Salut à tous ! Ca faisait longtemps que vous nous le demandiez et que nous vous le promettions, alors voilà qui devrait vous faire plaisir :...
tags:
- solarus
title: 'Z.A.P. : un screenshot par jour !'
---

Salut à tous !

Ca faisait longtemps que vous nous le demandiez et que nous vous le promettions, alors voilà qui devrait vous faire plaisir : nous avons décidé de publier sur le site une nouvelle image de la démo de Zelda Solarus 2 (ZAP) chaque jour !

Un nouveau screenshot est donc d'ores et déjà disponible dans la [galerie d'images](http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=scr) de la démo.

Nous allons donc mettre en ligne une nouvelle image de notre création tous les jours pendant environ un mois. Vous aurez donc de quoi vous faire patienter avant la démo ;-)

Accéder à la rubrique de [Z.A.P.](http://www.zelda-solarus.com/jeux.php?jeu=zf)
