---
date: '2015-07-18'
excerpt: Hier soir a été officiellement posée la première pierre de la principale ville de Mercuris' Chest ! Au cours d'une soirée de mapping en...
tags:
- solarus
title: Première pierre
---

Hier soir a été officiellement posée la première pierre de la principale ville de Mercuris' Chest !

Au cours d'une soirée de [mapping en live-streaming](https://www.livecoding.tv/video/zelda-mercuris-chest-mapping-avec-newlink/) en compagnie de Newlink, nous avons en effet inauguré la construction d'une immense et prospère ville.

![town](town-300x225.png)

Un grand bravo à Newlink qui a réalisé énormément de magnifiques graphismes inédits, comme ceux que vous voyez sur cette capture d'écran. Nous allons pouvoir les utiliser pour cette ville mais aussi pour l'ensemble du jeu.

À bientôt pour d'autres infos !
