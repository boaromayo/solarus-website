---
date: '2005-12-24'
excerpt: "En cette période de fêtes, une étape importante vient d'être franchie dans le développement de notre projet Zelda : Mercuris' Chest. Après..."
tags:
  - solarus
title: "Zelda Mercuris' Chest : la première map !"
---

En cette période de fêtes, une étape importante vient d'être franchie dans le développement de notre projet Zelda : Mercuris' Chest. Après déjà plusieurs années consacrées à la création d'un moteur de jeu évolué et de deux démos avec The Games Factory et Multimedia Fusion, nous venons d'entrer dans la phase de conception du jeu complet. Bien sûr, ça fait longtemps que le scénario est en préparation. Mais je suis fier de vous annoncer qu'il y a quelques jours, nous avons commencé la toute première scène du vrai jeu complet :D

Ceci est la toute première capture d'écran du jeu final :

![](http://www.zelda-solarus.com/images/zf/zmc/intro.png)

Cette image est tirée de l'intro. Bien entendu, ne comptez pas sur moi pour vous révéler quoi que ce soit sur ce qui se passe lors de l'intro ^\_^

Autre bonne nouvelle très importante pour le projet : un nouveau membre a rejoint l'équipe, il s'agit de Newlink. Newlink travaille en tant que graphiste. Il n'est pas le premier venu car il a déjà travaillé sur plusieurs projets comme Legacy of Cocolint, et Attack of the Shadow version Master Quest. Newlink a déjà de l'expérience en ce qui concerne la création de décors et de personnages dans le style de Zelda 3. Et en plus de cela il est très motivé pour bosser sur le projet Zelda M'C :)

Pour la création des maps du jeu complet, je m'occupe des graphismes de Zelda 3 et Newlink se charge d'y ajouter des éléments inédits. C'est ainsi que nous voulons aboutir à un projet basé sur les graphismes de Zelda 3 améliorés. Sur la capture d'écran de l'intro, vous pouvez déjà voir quelques décors inédits réalisés par Newlink, ainsi que les deux personnages, eux aussi inédits.

Voilà, après ces bonnes nouvelles je vous souhaite un joyeux Noël au nom de toute l'équipe !
