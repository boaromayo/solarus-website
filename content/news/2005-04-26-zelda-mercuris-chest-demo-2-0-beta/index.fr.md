---
date: '2005-04-26'
excerpt: Nous sommes encore le 26 avril, et comme promis voici la fameuse démo tant attendue ! Il s'agit d'une version bêta parce que nous n'avons pas...
tags:
- solarus
title: 'Zelda : Mercuris'' Chest démo 2.0 bêta !'
---

Nous sommes encore le 26 avril, et comme promis voici la fameuse démo tant attendue !

Il s'agit d'une version bêta parce que nous n'avons pas encore eu le temps de la tester vraiment à fond et parce que nous comptons sur vous pour nous signaler des bugs. Il semblerait notamment que jouer avec une manette ne soit pas encore possible. En tout cas, si vous trouvez des bugs, n'hésitez pas à m'en informer :)

Il est probable que la démo soit réactualisée dans les prochaines semaines au fur et à mesure des corrections de bugs, jusqu'à ce que l'on aboutisse à une version stable et définitive.

J'espère en tout cas que la démo vous plaira !

Update : et voilà, après quelques minutes de retard, la démo est vraiment disponible ! Amusez-vous bien !

[Télécharger la démo](http://www.zelda-solarus.com/download.php?name=zmcdemo)
