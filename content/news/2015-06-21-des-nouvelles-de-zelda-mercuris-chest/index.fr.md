---
date: '2015-06-21'
excerpt: Vous ne rêvez pas ! Ce n'est même pas le premier avril, pourtant c'est le moment de vous donner quelques nouvelles du projet qu'on vous promet...
tags:
- solarus
title: Des nouvelles de Zelda Mercuris' Chest
---

Vous ne rêvez pas !

Ce n'est même pas le premier avril, pourtant c'est le moment de vous donner quelques nouvelles du projet qu'on vous promet depuis si longtemps.

Au cours des derniers mois, vous n'êtes pas sans savoir que c'était plutôt sur le [nouvel éditeur de quêtes](http://www.zelda-solarus.com/zs/2015/05/solarus-1-4-disponible-nouvel-editeur-de-quetes/) et sur les [tutoriels vidéo](http://wiki.solarus-games.org/doku.php?id=fr:video_tutorial) que je travaillais. Ce qui intéresse beaucoup les nombreuses personnes souhaitant créer leur propre jeu avec Solarus, mais les joueurs que vous êtes n'aviez pas grand chose à vous mettre sous la dent ! Bref, pendant tout ce temps, Zelda Mercuris' Chest n'avançait pas.

Maintenant que le nouvel éditeur de quêtes est sorti, je passe beaucoup moins de temps sur son développement ou celui du moteur (je me contente essentiellement des corrections de bugs). Tout cela pour dire que le développement de Zelda Mercuris' Chest s'accélère ! Il y a beaucoup de travail car c'est un projet ambitieux, mais l'équipe est motivée. Je fais de temps en temps du live-coding, c'est-à-dire du développement et du mapping de Zelda Mercuris' Chest en direct vidéo. Si cela vous intéresse, visitez ma [chaîne Twitch](http://www.twitch.tv/christophozs) (pour les directs) et ma [chaîne YouTube](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufxKu9TItbmyvjlJ3eSdsW0a) (pour les extraits et les rediffusions). Mais attention aux spoilers !

Pour vous montrer un petit aperçu de l'avancement, voici une capture d'écran du village Mojo.

![Village Mojo de Zelda Mercuris' Chest](deku_village-300x225.png)

À bientôt pour d'autres nouvelles, je compte bien vous en donner régulièrement à partir d'aujourd'hui.
