---
date: '2013-04-13'
excerpt: Dix ans qu'on vous parle de ce projet. Il y a eu une démo, il a été recommencé, annulé et maintenant il redémarre. Comment présenter Zelda...
tags:
  - solarus
title: Mercuris' Chest de nouveau sur les rails
---

Dix ans qu'on vous parle de ce projet. Il y a eu une démo, il a été recommencé, annulé et maintenant il redémarre. Comment présenter Zelda Mercuris' Chest en quelques mots ?

![Tannek](tannek.jpg)
[Zelda Mercuris' Chest](http://www.zelda-solarus.com/zs//jeu/zelda-mercuris-chest/) (en français, le coffre de Mercuris :P) est le successeur de [Zelda Mystery of Solarus](http://www.zelda-solarus.com/zs//jeu/zelda-mystery-of-solarus-dx/) (maintenant DX). Mais ce n'est pas une suite au point de vue du scénario. L'histoire se déroulera bien à part. À vrai dire, une grande partie du scénario avait déjà été écrite lors de la première vie de ce projet, mais on vous a pas dévoilé grand chose si ce n'est quelques [artworks](http://www.zelda-solarus.com/zs/article/zmc-images/).

La [démo](http://www.zelda-solarus.com/zs/article/zmc-telechargements/), à laquelle on vous conseille de jouer, comporte un immense donjon dont le but était de servir de mise en bouche pour le jeu complet. Il n'y a pas de scénario : le seul but est de terminer le donjon ! Ce donjon, intitulé le Temple du Rail, avait pour thème les wagonnets et les rails. Un parcours semé d'embûches doit être traversé pour vous mener, vous et vos wagonnets, jusqu'à la bataille épique contre Colossius, le boss du donjon.

![zap09](zap09-300x225.png)

(Rappelons d'ailleurs que ce temple tout entier fait un clin d'oeil à Perfect Dark, célèbre jeu de tir subjectif de la Nintendo 64, en reprenant exactement les mêmes salles que le niveau Secteur 51 !)

Le Temple du Rail sera inclus dans le jeu complet, je peux vous l'annoncer ! Et sous une autre forme remasterisée : il sera situé au fond d'une ancienne mine, un endroit qui aura son importance dans le scénario. Ces captures d'écrans viennent donc du nouveau Temple du Rail :

![](zmc_mine_1-300x225.png)

![](zmc_mine_2-300x225.png)

Cette version remasterisée du Temple du Rail est dotée de graphismes inédits que nous avons créés avec amour, spécialement pour faire un style de mine abandonnée. Au niveau de l'avancement, les maps du donjon sont en cours de reproduction avec l'éditeur de map. Mymy m'aide beaucoup pour cette tâche ; merci aussi à Angenoir37 et Miniriël qui ont un peu travaillé dessus en décembre.  La reproduction des salles à l'identique est presque terminée. Ensuite je passerai à la partie programmation pour que le donjon devienne jouable. Les testeurs pourront donc bientôt essayer :)

En parallèle, d'autres parties du jeu avancent, mais ce sera pour une autre mise à jour ! À très bientôt pour d'autres nouvelles.
