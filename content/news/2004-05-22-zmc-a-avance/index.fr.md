---
date: '2004-05-22'
excerpt: Jusque là, vous n'aviez pas beaucoup d'infos concernant notre projet ZMC (titre toujours aussi énigmatique), nous y remédions avec cette annonce :...
tags:
- solarus
title: 'ZMC : Ça avance'
---

Jusque là, vous n'aviez pas beaucoup d'infos concernant notre projet ZMC (titre toujours aussi énigmatique), nous y remédions avec cette annonce : nous modélisons en ce moment les maps du jeu mais nous faisons cela intelligemment pour accroître la rapidité de création et l'optimisation en matière de mémoire.

D'ici quelques semaines, un nouveau logo devrait arriver ainsi que des informations supplémentaires et pourquoi pas... des captures du jeu ! Si j'étais vous, je reviendrai souvent.

Je rappelle également qu'à l'instar de Majora's Mask, nous orientons l'aventure sur un nombre plus important de quêtes annexes, le principal défaut du jeu Zelda Solarus, peut-être au détriment du nombre de donjons...

[Section consacrée au jeu (ZAP et ZMC)](jeux.php?jeu=zf)
