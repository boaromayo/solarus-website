---
date: '2013-06-04'
excerpt: Une troisième solution vient d'arriver pour Zelda Solarus DX. Cette fois-ci, c'est sous la forme d'un guide au format PDF basé sur la solution...
tags:
- solarus
title: Guide Officiel Stratégique de Zelda Solarus DX
---

Une troisième solution vient d'arriver pour Zelda Solarus DX. Cette fois-ci, c'est sous la forme d'un guide au format PDF basé sur la solution texte de Renkineko. Vous y trouverez donc exactement le même contenu que cette dernière, à la différence de la mise en page fait par moi-même. Cette nouvelle solution peut être pratique pour jouer sans connexion à Internet. Elle n'est disponible pour le moment qu'en français. Je vous souhaite à toutes et à tous une bonne lecture !
![Couverture](couverture-de-la-solution.jpg)

[Télécharger le guide PDF de ZSDX](http://www.zelda-solarus.com/zs/wp-content/uploads/2013/06/Solution%20Zelda%20Solarus%20DX.pdf)
