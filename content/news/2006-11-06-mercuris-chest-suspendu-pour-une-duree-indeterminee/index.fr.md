---
date: '2006-11-06'
excerpt: En ces temps où l'actu consacrée à Twilight Princess est omniprésente et nous rend de plus en plus impatients, je suis désolé de casser...
tags:
  - solarus
title: Mercuris' Chest suspendu pour une durée indéterminée
---

En ces temps où l'actu consacrée à Twilight Princess est omniprésente et nous rend de plus en plus impatients, je suis désolé de casser l'ambiance avec une mauvaise nouvelle... :P

Non, ce n'est pas une blague. Le projet Zelda : Mercuris' Chest, dont nous sommes si fiers de vous parler depuis de longs mois, voit son développement officiellement suspendu jusqu'à nouvel ordre.

Il faut savoir que je n'ai pas touché au projet depuis le mois de juillet. Si je ne donnais plus d'infos, ce n'était pas parce que je voulais garder tout secret, mais uniquement parce que je n'avançais pas. Alors il est temps de faire un bilan et de réfléchir à l'avenir du projet. En annonçant que le projet est suspendu, je ne fais qu'officialiser une situation qui dure depuis plusieurs mois.

Mais rassurez-vous : **le projet n'est pas annulé** ! J'ai juste décidé de l'arrêter provisoirement, pour plusieurs raisons. D'abord, un manque de temps dû à mes études qui me donnent énormément de travail cette année. Manque de temps dû aussi à de nombreuses activités extérieures à Zelda Solarus ou à l'informatique en général. Et enfin, quand je peux je travaille également sur d'autres projets très intéressants... Pour résumer la situation, disons que ma vie personnelle a pris le dessus sur mon activité de création de Zelda M'C.

Mais d'autres facteurs m'incitent à ne pas reprendre le développement tout de suite. D'une part, il y a le nouveau design de Zelda Solarus qui est en cours de création. Il a été réalisé par plusieurs personnes et il est en cours de finition. Sa mise en place s'accompagnera de gros travaux de nettoyage du code XHTML et PHP du site, des travaux qui auraient dû être faits depuis longtemps. Ce design promet d'être original par rapport aux autres sites. Il sera d'ailleurs dédié à Zelda : Mercuris' Chest ^\_^

Deuxièmement, il y a bien sûr l'arrivée imminente de Twilight Princess. Avec toute l'équipe nous comptons bien vous offrir des mises à jour à la hauteur de l'événement. Souvenez-vous de la sortie de The Wind Waker : 24 heures plus tard, nous avions déjà publié le début de la soluce. A l'occasion de la sortie de la Wii et de Zelda TP, toute l'équipe sera mobilisée pour couvrir l'événement.

Voilà. Pour conclure, cette pause dans le développement de Mercuris' Chest est une mauvaise nouvelle, mais elle est nécessaire. La création d'un tel projet est un travail de longue haleine. Quand on voit les difficultés que l'on rencontre, on réalise mieux la performance que c'est de sortir un jeu complet.

L'avenir du site dans les prochaines semaines, c'est donc un nouveau design et la sortie de Twilight Princess. Mercuris' Chest reviendra aux environs du mois de mars... :)
