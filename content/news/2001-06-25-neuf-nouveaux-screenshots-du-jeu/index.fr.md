---
date: '2001-06-25'
excerpt: 'Voici rien que pour vos yeux les nouveaux screenshots de Zelda : Mystery of Solarus. Vous pouvez voir sur certaines images quelques nouveautés... Je...'
tags:
  - solarus
title: Neuf nouveaux screenshots du jeu
---

Voici rien que pour vos yeux les nouveaux screenshots de Zelda : Mystery of Solarus. Vous pouvez voir sur certaines images quelques nouveautés... Je ne vous en dis pas plus mais je vous laisse regarder.

[](solarus-ecran15.png)

[](solarus-ecran16.png)

[](solarus-ecran17.png)

[](solarus-ecran18.png)

[](solarus-ecran19.png)

[](solarus-ecran20.png)

[](solarus-ecran21.png)

[](solarus-ecran22.png)

[](solarus-ecran11.jpg)

Si vous voulez en savoir plus sur l'avancement du jeu, n'hésitez à me contacter.
