---
date: '2009-11-10'
excerpt: A 38 jours de la sortie de la démo de Zelda Mystery of Solarus DX, je vous propose aujourd'hui d'en savoir plus sur l'équipe.
tags:
  - solarus
title: L'équipe de Zelda Mystery of Solarus DX
---

A 38 jours de la sortie de la démo de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx), je vous propose aujourd'hui d'en savoir plus sur l'équipe, c'est-à-dire tous ceux qui participent ou ont participé au projet jusqu'ici, et le rôle de chacun.
La page "équipe" vient en effet d'être mise à jour, elle est plus complète et surtout plus détaillée qu'auparavant.
Vous y verrez que si environ 3 membres travaillent de manière quotidienne sur le projet, le total des contributions monte à 27 personnes en comptant les tests, les traductions, les portages et bien sûr ceux qui ont donné un coup de main dans le passé.

- [L'équipe de Zelda Solarus DX](http://www.zelda-solarus.com/jeu-zsdx-equipe)

Merci et bravo à toute l'équipe pour votre aide :)
