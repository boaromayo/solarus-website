---
date: '2015-12-20'
excerpt: Here is a small update of our games! Not a huge one, but it fixes the Italian translation of ZSDX and a few other issues. Changes in Zelda ROTH SE...
tags:
- solarus
title: Another update of our games
---

## Here is a small update of our games! Not a huge one, but it fixes the Italian translation of ZSDX and a few other issues. Changes in Zelda ROTH SE 1.0.8

- Increase rupee drop rates (#86).
- Increase chances of getting bombs before dungeon 1.
- Fix inappropriate English translation.

## Changes in ZSDX 1.10.3

- Fix Italian dialogs exceeding the dialog box (thanks Marco).
- Fix game-over stopped sometimes using a workaround (#87).

## Changes in ZSXD 1.10.3

- Fix game-over stopped sometimes using a workaround.
- Fix dialog cursor drawn at wrong position after successive questions.

Enjoy!
