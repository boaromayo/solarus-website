---
date: '2001-02-02'
excerpt: Je travaillais sur ce grand projet dans le secret le plus complet depuis un mois déjà, et aujourd'hui nous vous le dévoilons officiellement. Il...
tags:
  - solarus
title: Le projet est dévoilé !
---

Je travaillais sur ce grand projet dans le secret le plus complet depuis un mois déjà, et aujourd'hui nous vous le dévoilons officiellement. Il s'agit d'un nouvel épisode de la série mythique des Zelda. Je développe ce jeu à l'aide du logiciel RPG Maker 2000. Pour l'instant, je n'en suis qu'au début du jeu (pour vous faire une idée, il devrait y avoir une dizaine de donjons, et je commencerai bientôt le premier). Les combats seront en temps réels (comme dans les Zelda) et non en écran à part (comme dans les RPG style Final Fantasy). Les graphismes sont tirés à 99% de Zelda 3 sur SNES. Je sais que vous en mourez d'envie, alors voilà, je vous donne les premiers écrans (cliquez pour agrandir) :

[](solarus-ecran1.jpg)

[](solarus-ecran2.jpg)

[](solarus-ecran3.jpg)

[](solarus-ecran4.jpg)

[](solarus-ecran5.jpg)

[](solarus-ecran6.jpg)

Comme vous pouvez le constater, la qualité graphique est celle de Zelda 3. En ce qui concerne les musiques, j'ai sélectionné les meilleures mélodies de Zelda 3 et de Zelda DX. Le scénario n'est pas encore vraiment créé, mais le principal de l'histoire est que Zelda et ses neuf enfants ont été enlevés et c'est évidemment vous qui êtes chargé de les délivrer... Ce n'est pas très original mais si jamais vous avez des idées, écrivez-moi. La première démo du jeu devrait être disponible dans le courant de la semaine prochaine. Quant à la date de sortie finale, je ne préfère pas la préciser car c'est très long de faire le jeu et je n'ai aucune idée de quand il sera fini. Tenez-vous au courant !
