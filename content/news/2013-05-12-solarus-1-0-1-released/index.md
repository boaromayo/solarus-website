---
date: '2013-05-12'
excerpt: '&nbsp; An update of Solarus 1.0 is available! This update fixes a bunch of bugs of previous versions (some of them were very old!), and...'
tags:
- solarus
title: Solarus 1.0.1 released
---

An update of Solarus 1.0 is available!

This update fixes a bunch of bugs of previous versions (some of them were very old!), and improves the documentation as well as the quest editor. It is recommended to upgrade.

Because it is a patch release (1.0.0 to 1.0.1), there is no change in the format of data files or in the Lua API, everything remains compatible.

- [Download Solarus 1.0.1](http://www.solarus-games.org/downloads/download-solarus/ "Download Solarus")
- [Solarus Quest Editor](http://www.solarus-games.org/solarus/quest-editor/ "Solarus Quest Editor")
- [Lua API documentation](http://www.solarus-games.org/doc/1.0.0/lua_api.html)

## Changes in Solarus

- Fix the Mac OS X port.
- Fix jump movement accross two maps ending up in a wall (#189).
- Fix a possible crash in TextSurface.
- Fix the hero disappearing a short time after using the sword (#35).
- Fix the boomerang failing to bring back pickables sometimes (#187).
- Fix parallax scrolling tiles not always displayed (#167).
- Fix the setting joypad_enabled that had no effect (#163).
- Fix doors not working when they require equipment items.
- Fix a possible compilation warning in Surface.cpp.
- Fix creating a transition from the callback of a previous one.
- Fix crystal blocks animated late when coming from a teletransporter (#61).
- Fix arrows that got stopped when outside the screen (#73).
- Fix diagonal movement that failed in narrow passages (#39).
- Don't die if a script makes an error with a sprite (#151).
- Don't die if a script makes an error with an enemy attack consequence.
- Allow enemies to lose 0 life points when attacked (#137).
- Pixel-precise collisions can now also be performed on 32-bit images.

## Changes in Solarus Quest Editor

- Add the possibility to show or hide each type of entity (#60).
- Keep the map coordinates shown when changing the zoom (#183).
- Fix the map view not updated correctly when changing the zoom (#174).
- Show the correct sprite of destructible objects (#77).
- Show an appropriate error message if the LuaJ jar is missing (#173).
- Fix the title bar string (#176).

## Documentation changes

- Split the [C++ documentation](http://www.solarus-games.org/developer_doc/latest/) and the [quest data files documentation](http://www.solarus-games.org/doc/latest/quest.html) (#181).
- Add a search feature to the documentation pages.

Enjoy!
