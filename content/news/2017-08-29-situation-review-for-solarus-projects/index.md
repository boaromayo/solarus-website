---
date: '2017-08-29'
excerpt: "This summer, the Solarus Team has been working hard on multiple projects in parallel. On the French website, The Legend of Zelda: Mercuris' Chest is..."
tags:
  - solarus
title: Situation Review for Solarus projects
---

![](solarus_team_logo_text.png)

This summer, the Solarus Team has been working hard on multiple projects in parallel. On the French website, _The Legend of Zelda: Mercuris' Chest_ is the one we talk the most, although screenshots are rarer these days. However, this is not the only project that makes progess currently. It is now the time to unravel what we want to achieve in the near future. You might follow Chris on his [Twitter account](https://twitter.com/ChristophoZS) or [Youtube streamings](https://www.youtube.com/user/ChristophoZS/live). If so, you already know some news.### The Legend of Zelda: Mercuris Chest

This is Solarus Team's flagship. It is an ambitious project, with a deep story, a huge overworld and lots of dungeons. The story is written, the mapping is progressing. Newlink created a lot of new sprites and tiles for this project, including a custom Link. Here is a screenshot of a very important village in the game, but we can't tell you more!

![](mc_screen-300x226.png)

### The Legend of Zelda: Oni-Link Begins Solarus Edition

![](olb_se_logo-300x205.png)

Following [Return of the Hylian Solarus Edition](http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/), this is the remake of Vincent Jouillat's second game. Lately, we have progressed a lot on this remake, during live-streaming nights. We can estimate that 40% is done. Although all the enemy sprites, NPC sprites, and Oni-Link sprites are done, there is still a lot of mapping to do. We had the secret hope of releasing it on August 12th (Vincent's games birthdays) but we choose to not rush it and take time, without giving up the other projects.

![](olb_n1.png)

### The Legend of Zelda: Link's Awakening - A Link to the Dream

![](mabe_village.png)

It is a remake of _Link's Awakening_ in _A Link to the Past_ graphical style. This project was stopped a few years ago, but since a few weeks a born again from its ashes. It looks very promising.This is Binbin's project ([Zeldaforce](https://zeldaforce.net/) webmaster). He is almost a member of the Solarus Team, actually he is the author of our [French speaking website](http://www.zelda-solarus.com/zs/), so we can say he's in the team! The Solarus Team is helping him to get this project done as soon as possible.You will find more information (French-speaking) on [his website](https://zeldaforce.net/jeux/a-link-to-the-dream/).### Children of Solarus

![](ChildrenOfSolarus-logo-highres-300x130.png)

For once, let's take a break from _The Legend of Zelda_ series, and let's take a look at what is our security belt against obvious copyright issues. As you may know, using _A Link to the Past_ assets is not allowed, neither making and distributing your own _The Legend of Zelda_ game. So Diarandor works hard to create 100% free assets (Creative Commons): graphics and sound/music, without any reference to _The Legend of Zelda_ series. The result is _Children of Solarus_, a 100% free and open-source version of _Mystery of Solarus DX_.

![](eldran.png)

This screenshot is not from the game, but from a test project used by Diarandor to develop graphics and scripts. The game will have several changes and enhancements from Mystery of Solarus DX. Diarandor is currently working on the outside overworld tileset. As soon as this is ready for mapping, Chris will begin making the maps and will live-stream it.

### Solarus Quest Editor

![Solarus Quest Editor Logo](sqe-logo-with-text-white-background-300x60.png)

If you see more and more projects using Solarus, that is because each Solarus iteration brings a lot of new features, and Solarus Quest Editor becomes easier and easier to use. We are working on the next Solarus version (Solarus 1.6), whose main new feature will be a functionality that mappers dream of: **autotiles**! This functionality enabled automatic generation of walls around a room, for example. RPG Maker users should already know this feature well.

![](autotiles.png)

The goal is to make dungeon creation very easy: just draw the ground, and corresponding borders and walls are automatically added.### A new Solarus website and tutorial

As you might have seen, this current website is a bit bulky and old. Moreover, the written tutorials (originally made by Renkineko) are also old and sometimes deprecated. We want to give Solarus a more professional looking website. The wiki will be dropped, in favor of a [Grav-like tutorial](https://learn.getgrav.org/), made with [Grav](https://getgrav.org/), just like the new website. The benefit of using Grav over Wordpress is easy versioning of pages, because it is based on a markdown file-system instead of a database.

### English translation for XD 2: Mercuris Chess

![](DDRlrQ5WAAEvVCe.jpg-large.jpg)

On April 1st, we released [the sequel to MoS XD](http://www.solarus-games.org/games/zelda-xd2-mercuris-chess/). However, it is still only available in French. In order to translate it to other languages, we need English first. It'll be a tough task because a lot of jokes are references to French pop culture (cult movies for instance). There are lots of dialogs by funny NPCs all around the overworld. If you can help us, don't hesitate! Come over chat with us [on Gitter](https://gitter.im/solarus-games/Lobby). And... that's all for today!
