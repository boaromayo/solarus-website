---
date: '2012-04-08'
excerpt: 'Vous les attendiez avec impatience : deux nouveaux épisodes de la soluce vidéo de Zelda Mystery of Solarus DX.'
tags:
  - solarus
title: La soluce continue
---

Vous les attendiez avec impatience : deux nouveaux épisodes de la soluce vidéo de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx) viennent d'être publiés.

- [Épisode 9 : du niveau 4 au niveau 5](http://www.youtube.com/watch?v=x84d8Lh24Mg)

- [Épisode 10 : niveau 5 - Ancien Château](http://www.youtube.com/watch?v=oeNE68E4xmk)

Nous en sommes à peu près à la moitié du jeu, à la fin du cinquième donjon. Merci à Elenya, qui est en train de traduire le jeu en allemand et qui m'a accompagné aux commentaires pour ces deux épisodes ! :)
