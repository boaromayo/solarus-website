---
date: '2006-01-01'
excerpt: Toute l'équipe de Zelda Solarus vous présente ses meilleurs voeux pour l'année 2006 ^_^ L'année 2005 a été marquée par la sortie de Four...
tags:
- solarus
title: Bonnée année 2006 !
---

Toute l'équipe de Zelda Solarus vous présente ses meilleurs voeux pour l'année 2006 ^\_^

L'année 2005 a été marquée par la sortie de Four Swords Adventures, de la Nintendo DS, et par des tas d'informations et d'images sur Twilight Princess. Pour Zelda Solarus, c'est aussi l'avancement de notre projet Zelda Mercuris' Chest qui a marqué l'année : d'abord l'annonce du titre le premier avril, puis la sortie de la deuxième démo le 26 avril, et enfin la version anglaise de cette démo le 10 août. 2005 a aussi été l'année du changement puisque le site étant de plus en plus visité, nous sommes passés d'un hébergement mutualisé à un serveur dédié pour vous offrir de meilleurs services.

L'année 2006 s'annonce très remplie elle aussi avec la sortie de Zelda : Twilight Princess et de la Revolution. Sur Zelda Solarus, nous vous donnerons régulièrement des informations sur l'avancement de Zelda M'C ainsi que de nombreuses captures d'écran, au fur et à mesure de la création des scènes. Bien entendu, les mangas, les soluces et les créations de fans continueront d'être mis à jour le plus souvent possible. Vous aurez aussi certainement droit à quelques surprises ;)

Bonne et heureuse année 2006 à tous nos visiteurs. Espérons que 2006 soit encore meilleure que 2005 et que Zelda Solarus continue encore longtemps :)
