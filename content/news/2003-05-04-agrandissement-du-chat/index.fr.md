---
date: '2003-05-04'
excerpt: Il faut bien le reconnaùtre, notre chat IRC (dialogue en direct) était toujours désert ou presque jusqu'à maintenant. Pour y remédier, nous...
tags:
- solarus
title: Agrandissement du chat !
---

Il faut bien le reconnaître, notre chat IRC (dialogue en direct) était toujours désert ou presque jusqu'à maintenant. Pour y remédier, nous avons fusionné avec le canal #zelda, que nous partageons désormais avec deux autres grands sites sur Zelda : [Puissance-Zelda](http://www.puissance-zelda.com) et [Zelda-Boss](http://www.zelda-boss.fr.st). Nous faisons maintenant partie du plus grand chat francophone sur Zelda !

Bien sûr, nous continuerons à faire des soirées chat, y compris sur le thème de Zelda : Advanced Project, l'avantage est que maintenant il y aura du monde !

[Accéder au chat](http://connexion.asterochat.com/?id=23857)
