---
date: '2012-04-22'
excerpt: Ça continue ! Voici deux nouveaux épisodes de la soluce vidéo de Zelda Mystery of Solarus.
tags:
  - solarus
title: "Solution du Dédale d'Inferno "
---

Ça continue !
Voici deux nouveaux épisodes de la [soluce vidéo de Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx-soluce).
Cette fois-ci, c'est avec Neovyse que j'ai eu le plaisir de réaliser les commentaires. :)

- [Épisode 11 : du niveau 5 au niveau 6](http://www.youtube.com/watch?v=jbUnejEjXrs)

- [Épisode 12 : niveau 6 - Dédale d'Inferno](http://www.youtube.com/watch?v=NoAIuV9d4qA)

On commence donc à arriver dans les donjons vraiment difficile. J'espère que ça vous aidera, mais comme toujours, n'abusez pas des solutions, consultez-les seulement si vous êtes vraiment bloqués ! ^\_^
