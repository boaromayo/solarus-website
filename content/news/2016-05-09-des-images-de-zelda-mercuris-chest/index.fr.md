---
date: '2016-05-09'
excerpt: Bonjour à toutes et à tous, Le développement de notre projet Zelda Mercuris' Chest continue ! Avec Solarus Quest Editor, Newlink se met au...
tags:
- solarus
title: Des images de Zelda Mercuris' Chest
---

Bonjour à toutes et à tous,

Le développement de notre projet [Zelda Mercuris' Chest](http://www.zelda-solarus.com/zs/article/zmc-presentation/) continue !

Avec Solarus Quest Editor, Newlink se met au mapping pour notre plus grand bonheur.

Voici quelques images de la principale ville du jeu :

![town_4](town_4-300x225.png)
![town_3](town_3-300x225.png)
![town_2](town_2-300x225.png)

Vous l'aurez peut-être remarqué, le graphisme de Link a été personnalisé ! Merci à Newlink qui n'a jamais aussi bien porté son nom :)

Il n'y a pas encore les autres personnages de la cité, mais ce sera une ville animée et vaste. Il y aura beaucoup de mini-quêtes !

Newlink et moi avons aussi réalisé une forêt un peu mystérieuse. Je ne vous en dis pas plus, voici plutôt les images :

![torin_forest_2](torin_forest_2-300x225.png)
![torin_forest_1](torin_forest_1-300x225.png)

On travaille donc actuellement sur le monde extérieur. Newlink a beaucoup d'idées et chaque région aura son propre thème visuel. Il y a encore beaucoup de boulot mais cette fois-ci ça avance pour de bon !
