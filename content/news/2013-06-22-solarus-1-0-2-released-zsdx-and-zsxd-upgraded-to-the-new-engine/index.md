---
date: '2013-06-22'
excerpt: An update of Solarus 1.0, Zelda Mystery of Solarus DX and Zelda Mystery of Solarus XD is available! This update fixes a bunch of bugs of previous...
tags:
- solarus
title: Solarus 1.0.2 released, ZSDX and ZSXD upgraded to the new engine
---

An update of Solarus 1.0, Zelda Mystery of Solarus DX and Zelda Mystery of Solarus XD is available!

This update fixes a bunch of bugs of previous versions. It is recommended to upgrade.

Because it is a patch release (1.0.1 to 1.0.2), there is no change in the format of data files or in the Lua API, everything remains compatible.

- [Download Solarus 1.0.2](http://www.solarus-games.org/downloads/download-solarus/ "Download Solarus")
- [Solarus Quest Editor](http://www.solarus-games.org/solarus/quest-editor/ "Solarus Quest Editor")
- [Lua API documentation](http://www.solarus-games.org/doc/1.0.0/lua_api.html)

And our games Zelda Mystery of Solarus DX and Zelda Mystery of Solarus XD are now fully working with Solarus 1.0. They are both released in a new version 1.6. There is also a new title screen from Neovyse (thanks!) and the English translation of both games has been greatly improved (thanks ibara!).

We have worked hard to upgrade both games to the new Lua API (thanks for your help pdewacht!). This does not change much things for the players, but if you are developping a quest with Solarus, you might be interested in reusing and modifying some of our scripts, like the HUD, the pause menu, the title screen or some enemies. The Lua scripting API of Solarus 1.0 allows to customize all of this!

## Changes in Solarus 1.0.2

- Fix a crash when a treasure callback changes the hero's state (#224).
- Fix a crash when a victory callback changes the hero's state.
- Fix a crash due to invalid sprite frame when animation is changed (#26).
- Fix an assertion error with FollowMovement of pickables.
- Fix the fullscreen mode not working on Mac OS X 10.7+ (#213, #220).
- Fix pickable treasures that could be obtained twice sometimes.
- Fix fade-in/fade-out effects on sprites that did not work (#221).
- Fix sol.audio.play_music() that failed with "none" or "same" (#201).
- Fix item:set_sound_when_brandish() that did not work.
- Fix diagonal movement that could bypass sensors since Solarus 1.0.1.
- Fix circle movement not working after entity:set_enabled(true).
- Fix detection of movement finished for NPCs.
- Fix memory issues with menus (#210).
- Fix handling of nil parameter in boolean setters (#225).
- Fix hangling the default language.
- Correctly suspend timers when set_suspended_with_map is called.
- When a sprite is suspended, suspend its transitions (#226).
- Don't die if a music or a sound cannot be found.
- Don't die if an image cannot be found.
- Don't die if running a savegame without starting location specified.
- Don't die if a script refers to a non-existing equipment item.
- Don't die if the self parameter is missing when calling a method (#219).
- Fix dangling pointers after removing some kind of entities.

## Changes in Solarus Quest Editor 1.0.2

- Allow to create map entities from the quest tree (#208).
- Fix a typo in the bomb flower sprite (#214).
- Fix a possible NullPointerException when opening an invalid map.

## Documentation changes

- Add the syntax specification of maps and tilesets.

## Changes in Zelda Mystery of Solarus DX 1.6

- Improve the English translation.
- New title screen from Neovyse (#45).
- Fix a typo in French dialogs.
- Fix a door bug that could get the hero stuck in empty rooms.
- Fix saving the state of Billy's door.
- Dungeon 1: keep the final door open when coming back in the boss room.
- Dungeon 7 2F: save the nort-west torches.
- Savegames menu: the joypad was not working to delete a savegame.
- Dungeon 10 B1: fix a cauldron the hero could walk on (#20).
- Rupee house: don't let the player to get the piece of heart with the sword.
- Dungeon 6 2F: move crystal blocks to avoid a possible breach.
- Outside world B3: fix a minor tile error (#12).
- Dungeon 8 B1: fix a minor tile error (#2).
- Dungeon 8 B3: fix a minor tile error.

## Changes in Zelda Mystery of Solarus XD 1.6

- Improve the English translation.
- Fix the hero stuck after jumping on a block in dungeon 2.
- Fix all missing empty lines in dialogs.
- Add the Solarus engine logo at startup.
