---
date: '2002-04-14'
excerpt: Un nouveau fond d'écran nous a été envoyé par un de nos visiteurs (jcj39) que nous remercions ! N'hésitez pas vous aussi à nous envoyer vos...
tags:
- solarus
title: Nouveau fond d'écran
---

Un nouveau fond d'écran nous a été envoyé par un de nos visiteurs (jcj39) que nous remercions ! N'hésitez pas vous aussi à nous envoyer vos créations qu'elle soient graphiques, sonores ou textuelles (fictions ou autres), nous nous ferons une joie de les publier sur ZS.

![](zs2mini.jpg)

[Voir les fonds d'écrans](http://www.zelda-solarus.com/wallpapers.php3)
