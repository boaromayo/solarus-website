---
date: '2004-12-24'
excerpt: 'Je vous avais promis dans la newsletter une annonce importante concernant Zelda M''C... Eh bien la voici : nous allons faire une nouvelle version...'
tags:
- solarus
title: 'Zelda M''C : la démo version 2.0 est en préparation !'
---

Je vous avais promis dans la newsletter une annonce importante concernant Zelda M'C... Eh bien la voici : nous

allons faire une nouvelle version de la démo ! Ce sera exactement la même démo, mais avec le moteur du jeu complet,

qui comporte déjà pas mal d'améliorations :

- Moteur de déplacement amélioré et plus maniable
- Possibilité de porter et de lancer des bombes
- Sous-écrans de pause identiques à ceux de la version complète
- Pas de scrolling pour passer d'une zone à une autre de la map
- Possibilité de configurer les touches du clavier
- Possibilité de jouer avec une manette et de configurer ses boutons
- Possibilité de choisir entre le mode plein écran et le mode fenêtré
- Pas de plantages intempestifs :)

Sans parler du nouvel écran-titre :

![](001.png)

Cette démo version 2.0 devrait sortir dans pas trop longtemps, au premier semestre 2005 :)

Joyeux Noël à tous !
