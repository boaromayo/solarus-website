---
date: '2013-07-05'
excerpt: Here is a small improvement that will ease the life of quest creators in Solarus 1.1, the future version of the engine.
tags:
  - solarus
title: Maps have now a default destination entity
---

Here is a small improvement that will ease the life of quest creators in Solarus 1.1, the future version of the engine.

You will be able to set a destination entity as the default one of the map. It means that whenever a [teletransporter](http://www.solarus-games.org/doc/1.1/lua_api_teletransporter.html) (or some [Lua code](http://www.solarus-games.org/doc/1.1/lua_api_hero.html#lua_api_hero_teleport)) sends the hero to a map without specifying a [destination entity](http://www.solarus-games.org/doc/1.1/lua_api_destination.html), the hero will arrive on the default destination. If no destination was explicitly set as the default one, then the first one of the map is the default one.

Many maps are very small in a game (like a house in a village) and contain only one destination. In these cases, you no longer have to give a name to the destination and to link a teletransporter to this destination name. In the map of the village, you can make a teletransporter that goes to that map and leave its destination parameter unspecified.

When you create a new savegame, the starting map and the starting destination on that map also become optional in Solarus 1.1. If you don't call [game:set_starting_location()](http://www.solarus-games.org/doc/1.1/lua_api_game.html#lua_api_game_set_starting_location), the game starts on the first map declared in the [resource list](http://www.solarus-games.org/doc/1.1/quest_resource_file.html) and with the default destination entity of that map.

This change is not the most important new feature of Solarus 1.1, but it will reduce the risk of errors and improve flexibility!
