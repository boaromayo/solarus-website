---
date: '2013-05-09'
excerpt: Guten tag! Comme promis, une nouvelle version de Zelda Mystery of Solarus DX (1.5.2) vient de sortir, de même qu'une nouvelle version de ZSXD...
tags:
- solarus
title: Mystery of Solarus DX en allemand
---

Guten tag!

Comme promis, une nouvelle version de Zelda Mystery of Solarus DX (1.5.2) vient de sortir, de même qu'une nouvelle version de ZSXD (1.5.3), le tout avec une mise à jour du moteur pour l'occasion (Solarus 0.9.3) ! Voici donc les principales nouveautés.

D'abord, quelques améliorations et corrections de bugs dans le moteur Solarus :

- Changement du répertoire des sauvegardes sur Mac OS X pour un répertoire plus standard. Il vous faudra déplacer vos fichiers manuellement pour continuer à jouer avec vos sauvegardes existantes.
- Amélioration du support de Mac OS X, Android, Pandora, Caanoo et autres plates-formes.
- Les images en format autre que 8 bits peuvent désormais aussi être utilisées pour les collisions au pixel près.
- Correction du bug des blocs qui ne se déplaçaient parfois qu'à moitié.
- Amélioration de certains types de mouvements sur les machines les plus lentes.
- Correction d'un blocage lors de l'utilisation du grappin sur un chou-péteur.

Cette mise à jour du moteur (Solarus 0.9.3) devrait être la dernière de la branche 0.9, branche qui devient obsolète maintenant que Solarus 1.0.0 est sorti. Les corrections mentionnées ci-dessus sont d'ailleurs déjà présentes dans Solarus 1.0.0.

Du côté de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/zs/article/zmosdx-telechargements/ "Téléchargements"), voici les nouveautés de la version 1.5.2, en plus des améliorations apportées par Solarus 0.9.3 :

- Version allemande disponible (merci Elenya !).
- Correction d'un personnage qui pouvait se superposer avec le héros dans le château.
- Correction d'un coffre qui pouvait se superposer avec le héros dans une grotte des montagnes.
- Corrections diverses dans les dialogues français, anglais et espagnols.

Et enfin, concernant [Zelda Mystery of Solarus XD](http://www.zelda-solarus.com/zs/article/zmosxd-telechargements/ "Téléchargements") n'est pas en reste avec une nouvelle version 1.5.3 :

- Donjon 2 : le fragment de cur pouvait être obtenu très facilement avec l'épée.
- Corrections diverses dans les dialogues français et anglais.
- Ajout de creepers dans la caverne du sac de bombes.

Les téléchargements de ces nouvelles versions sont pour le moment disponibles pour Windows ou sous forme de code source. Pour les autres systèmes, cela devrait arriver rapidement :)
