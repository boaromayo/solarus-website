---
date: '2001-09-07'
excerpt: Et oui, enfin une update !! Le scénario a subi de grosses modifications ces derniers temps. En effet, l'histoire des neuf frères et soeurs de...
tags:
- solarus
title: Quelques changements dans le scénario
---

Et oui, enfin une update !!

Le scénario a subi de grosses modifications ces derniers temps. En effet, l'histoire des neuf frères et soeurs de Zelda ne tenait pas debout. De plus le fait que les neuf aient été enlevés donnait lieu a un scénario extrêmement linéaire dans le jeu.

Alors voici l'essentiel des changements. Tout d'abord, les neuf enfants existent toujours mais ne font plus partie de la famille royale : ils ont été appelés par Zelda (pour diviser la Triforce en neuf morceaux). On ne sait d'ailleurs rien de plus sur ces neuf mystérieux enfants, mais on en découvrira plus durant le jeu... Ce qui donnera lieu à plusieurs rebondissements.

Autre gros changement dans le scénario : quatre des neuf enfants seuleument ont été capturés. Ce qui veut dire que les quatre premiers donjons sont consacrés à leur sauvetage. Après, le scénario connaît beaucoup de rebondissements et je prévois encore 5 ou 6 donjons...

Allez lire la page du [scénario](http://www.zelda-solarus.com/scenario.php3) pour le connaître plus en détail.

Parlez-en sur le [forum](http://www.zelda-solarus.com/forum.php3) !
