---
title: Téléchargement
excerpt: Téléchargez le moteur de jeu Solarus et ses outils d'aide à la création.
type: singles
layout: download
tags: [download, install, installer, executable, engine, player, windows, macos, mac, linux]
aliases:
  - /fr/engine
---

Selon que vous soyez un [joueur](#gamer-package) ou un [développeur de jeux](#game-developer-package), voici les paquets à télécharger.
