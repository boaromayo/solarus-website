---
title: Download
excerpt: Download the Solarus engine and its game-creation tools.
type: singles
layout: download
tags: [download, install, installer, executable, engine, player, windows, macos, mac, linux]
aliases:
  - /engine
---

Depending on whether you are a [gamer](#gamer-package) or a [game developer](#game-developer-package), here are the packages to download.
