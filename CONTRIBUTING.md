# Contributing

## Translations

The website is translated in English and French, and won't be translated to more languages.

To create a page:

1. Create a folder. Its name will be the slug.
2. Create an `index.md` file inside. This will be the English page.
3. Create an `index.fr.md` file inside. This will be the French page.
4. You can refer to the same resouces within that folder in the French and English pages.

If you can't translate the English page to French, just add this in the French page:

```yaml
---
redirectToDefault: true
---
```

## Adding a game

1. Create a directory in `content/games`, named with your game's title.

2. The directory must contain these files:

   - 📄 `index.md`: the English page.
   - 📄 `index.fr.md`: the French page.
   - 🏞️ `thumbnail.png`: the thumbnail image (300x200 pixels)
   - 📸 `screen*.png`: screenshots of your game (optional)

3. The `index.md` file must contain the following front matter:

   ```yaml
   ---
   age: Public of your game. Possibilities are ['all', 'warning', 'restricted']
   controls: A list of controls. Possibilities are ['touch', 'mouse', 'keyboard', 'joypad']
   developer: 'Solarus Team'
   download: Link to the .solarus file, or link to the game's page in a store. # optional
   downloadType: Nothing means it's a .solarus. Otherwise the kind of host platform. # optional
   excerpt: Short description of the game.
   genre: A list of genres to define your game, such as ['Action-RPG', 'Adventure']
   id: The unique id you choose for your game (e.g. the savegame id).
   languages: List of standard language codes for the game's translations, such as ['en', 'fr', 'it']
   license: List of licenses for the game, such as['GPL v3', 'CC-BY-SA 4.0', 'Proprietary (Fair use)']
   maximumPlayers: 1 (or more)
   minimumPlayers: 1
   initialReleaseDate: Release date of your game (if published) YYYY-MM-DD format. # optional
   latestUpdateDate: Latest update of your game (if published) YYYY-MM-DD format. # optional
   screenshots: ['screen1.png', 'screen2.png', 'screen3.png', 'screen4.png']
   solarusVersion: Minimal version of Solarus to run the game, such as '1.6.x'
   sourceCode: Link to the game's source code, if any. # optional
   thumbnail: thumbnail.png
   title: Title of the game.
   version: Version of the game, such as '1.2.3'
   walkthroughs: # optional
     - type: Type of the walkthrough. Possibilities are ['video', 'walkthrough']
       url: Link to the walkthrough.
   website: Link to the game's website, if any. # optional
   ---
   ```

4. Below the front matter, you can write the game's description in Markdown.

5. The `ìndex.fr.md` file must only contain what is different from the English page, i.e. you should normally have this front matter:

   ```yaml
   excerpt: The French translation of the English excerpt.
   ```

6. Also, you should translate the game's description. Keep it empty to use the English one.

## Updating data related to Solarus

To update the data related to Solarus itself (latest version, downloads, etc.), you need to edit the `data/solarus.json` file.

Ideally, this could be automated by the CI, but it's not yet the case.

### Solarus latest version

The format is the following:

- `latestRelease`: The date of the latest release, in ISO 8601 format (YYYY-MM-DD).
- `latestStableVersion`: The latest stable version of Solarus.

```json
{
  "latestRelease": "2023-04-01",
  "latestStableVersion": "2.0.0"
}
```

### Download packages

The download packages are 2 lists of objects, one for the launcher and one for the quest editor.

The format is the following:

- `platform`: The platform name, such as "Windows", "Linux", "macOS", "Android"...
- `url`: The download URL of the package (can be a GitLab release artifact).
- `version`: The version of the package.
- `size`: The size of the package in bytes.

```json
{
  "downloads": {
    "launcher": [
      {
        "platform": "Windows",
        "url": "https://download.solarus-games.org/package.zip",
        "version": "2.0.0",
        "size": 1234567890
      },
      ... // other platforms
    ],
    "questEditor": [
      {
        "platform": "Windows",
        "url": "https://download.solarus-games.org/package.zip",
        "version": "2.0.0",
        "size": 1234567890
      },
      ... // other platforms
    ]
  }
}
```
